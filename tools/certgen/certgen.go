package main

import (
	"crypto/ecdsa"
	"crypto/elliptic"
	"crypto/rand"
	"crypto/x509"
	"crypto/x509/pkix"
	"encoding/pem"
	"math/big"
	"os"
	"time"
)

func main() {
	rootcert, cacert, key, ocspCert, ocspKey := genCA()
	cacertPEM := &pem.Block{
		Type:  "CERTIFICATE",
		Bytes: cacert.Raw,
	}
	file, err := os.Create("cacert.pem")
	if err != nil {
		panic(err)
	}
	err = pem.Encode(file, cacertPEM)
	if err != nil {
		panic(err)
	}
	file.Close()
	rootcertPEM := &pem.Block{
		Type:  "CERTIFICATE",
		Bytes: rootcert.Raw,
	}
	file, err = os.Create("rootcert.pem")
	if err != nil {
		panic(err)
	}
	err = pem.Encode(file, rootcertPEM)
	if err != nil {
		panic(err)
	}
	file.Close()
	keyBytes, err := x509.MarshalECPrivateKey(key)
	if err != nil {
		panic(err)
	}
	keyPEM := &pem.Block{
		Type:  "PRIVATE KEY",
		Bytes: keyBytes,
	}
	file, err = os.Create("key.pem")
	if err != nil {
		panic(err)
	}
	err = pem.Encode(file, keyPEM)
	if err != nil {
		panic(err)
	}
	file.Close()
	ocspKeyBytes, err := x509.MarshalECPrivateKey(ocspKey)
	if err != nil {
		panic(err)
	}
	ocspKeyPEM := &pem.Block{
		Type:  "PRIVATE KEY",
		Bytes: ocspKeyBytes,
	}
	file, err = os.Create("ocsp_key.pem")
	err = pem.Encode(file, ocspKeyPEM)
	if err != nil {
		panic(err)
	}
	file.Close()
	ocspCertPEM := &pem.Block{
		Type:  "CERTIFICATE",
		Bytes: ocspCert.Raw,
	}
	file, err = os.Create("ocsp_cert.pem")
	if err != nil {
		panic(err)
	}
	err = pem.Encode(file, ocspCertPEM)
	if err != nil {
		panic(err)
	}
}

func genCA() (*x509.Certificate, *x509.Certificate, *ecdsa.PrivateKey, *x509.Certificate, *ecdsa.PrivateKey) {
	privKeyRoot, err := ecdsa.GenerateKey(elliptic.P521(), rand.Reader)
	if err != nil {
		panic(err)
	}
	privKeyCA, err := ecdsa.GenerateKey(elliptic.P256(), rand.Reader)
	if err != nil {
		panic(err)
	}
	privKeyOCSP, err := ecdsa.GenerateKey(elliptic.P256(), rand.Reader)
	if err != nil {
		panic(err)
	}
	template := &x509.Certificate{
		Subject: pkix.Name{
			CommonName: "Brick Test Root",
		},
		SignatureAlgorithm:    x509.ECDSAWithSHA256,
		SerialNumber:          big.NewInt(int64(1000)),
		NotBefore:             time.Now().UTC(),
		NotAfter:              time.Now().UTC().AddDate(5, 0, 0),
		KeyUsage:              x509.KeyUsageDigitalSignature | x509.KeyUsageKeyEncipherment | x509.KeyUsageCertSign,
		ExtKeyUsage:           []x509.ExtKeyUsage{x509.ExtKeyUsageClientAuth, x509.ExtKeyUsageServerAuth},
		IsCA:                  true,
		MaxPathLen:            1,
		BasicConstraintsValid: true,
	}
	rootCertDER, err := x509.CreateCertificate(rand.Reader, template, template, privKeyRoot.Public(), privKeyRoot)
	if err != nil {
		panic(err)
	}
	rootCert, err := x509.ParseCertificate(rootCertDER)
	if err != nil {
		panic(err)
	}
	template.Subject = pkix.Name{
		CommonName: "Brick Test CA",
	}
	template.SerialNumber = big.NewInt(int64(1001))
	template.MaxPathLen = 0
	template.MaxPathLenZero = true
	template.KeyUsage = x509.KeyUsageDigitalSignature | x509.KeyUsageKeyEncipherment | x509.KeyUsageCertSign | x509.KeyUsageCRLSign
	caCertDER, err := x509.CreateCertificate(rand.Reader, template, rootCert, privKeyCA.Public(), privKeyRoot)
	if err != nil {
		panic(err)
	}
	caCert, err := x509.ParseCertificate(caCertDER)
	if err != nil {
		panic(err)
	}
	template.SerialNumber = big.NewInt(int64(1002))
	template.MaxPathLen = 0
	template.MaxPathLenZero = true
	template.KeyUsage = x509.KeyUsageDigitalSignature | x509.KeyUsageCRLSign
	template.ExtKeyUsage = []x509.ExtKeyUsage{x509.ExtKeyUsageOCSPSigning}
	ocspCertDER, err := x509.CreateCertificate(rand.Reader, template, rootCert, privKeyOCSP.Public(), privKeyRoot)
	if err != nil {
		panic(err)
	}
	ocspCert, err := x509.ParseCertificate(ocspCertDER)
	if err != nil {
		panic(err)
	}
	return rootCert, caCert, privKeyCA, ocspCert, privKeyOCSP
}
