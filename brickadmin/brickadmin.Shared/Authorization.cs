﻿using System;
using System.Collections.Generic;
using System.Text;

namespace brickadmin.Shared
{
    public class Authorization
    {
        public string Id;
        public string Type;
        public string Value;
        public string Status;
        public IList<Challenge> Challenges;
    }
}
