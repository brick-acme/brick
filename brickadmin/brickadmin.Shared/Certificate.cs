﻿using System;
using System.Collections.Generic;
using System.Text;

namespace brickadmin.Shared
{
    public class Certificate
    {
        public string Cert { get; set; }
        public string RevocationDate { get; set; }
        public IList<string> CaCerts { get; set; }
    }
}
