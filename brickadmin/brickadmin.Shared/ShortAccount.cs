﻿namespace brickadmin.Shared
{
    public class ShortAccount
    {
        public string ID { get; set; }
        public string ExternalBinding { get; set; }
    }
}