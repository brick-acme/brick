﻿using System;
using System.Collections.Generic;
using System.Text;

namespace brickadmin.Shared
{
    public class Account
    {
        public string Id;
        public IEnumerable<string> Orders;
        public string Status;
        public IEnumerable<Authorization> Authorizations;
    }
}
