﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace brickadmin.Shared
{
    public class Order
    {
        public string Id;
        public string Status;
        public IList<Authorization> Authz;
        public string NotBefore;
        public string NotAfter;
        public string CertID { get; set; }
    }

}
