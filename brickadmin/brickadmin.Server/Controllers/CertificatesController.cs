﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using X509CertificateHelper;

namespace brickadmin.Server.Controllers
{
    [ApiController]
    [Route("api/certificates")]
    public class CertificatesController : ControllerBase
    {
        private ILogger logger;
        private Brick.StorageAuthority.StorageAuthority.StorageAuthorityClient client;

        public CertificatesController(ILogger<CertificatesController> l, Brick.StorageAuthority.StorageAuthority.StorageAuthorityClient c)
        {
            logger = l;
            client = c;
        }
        [Route("{id}")]
        [HttpGet]
        public async Task<ActionResult<brickadmin.Shared.Certificate>> GetCert([FromRoute]string id)
        {
            var grpcCert = await client.GetCertificateAsync(new Brick.Core.IdRequest() { Id = id });
            var cert = new X509Certificate2(grpcCert.CertDER.ToArray());
            var grpcCaCert = await client.GetCaCertificateAsync(new Brick.Core.IdRequest() { Id = grpcCert.CaCert.Id });
            var cacert = new X509Certificate2(grpcCaCert.CertDER.ToArray());
            var grpcRootCert = await client.GetCaCertificateAsync(new Brick.Core.IdRequest() { Id = grpcCaCert.CaCertId });
            var rootcert = new X509Certificate2(grpcRootCert.CertDER.ToArray());
            return new brickadmin.Shared.Certificate()
            {
                Cert = cert.PEM(),
                CaCerts = new List<string>(new[] { cacert.PEM(), rootcert.PEM() }),
                RevocationDate = String.IsNullOrWhiteSpace(grpcCert.RevocationTime) ? null : grpcCert.RevocationTime
            };
        }
        [HttpPost]
        [Route("{id}/revoke")]
        public async Task<IActionResult> RevokeCert([FromRoute]string id)
        {
            await client.RevokeCertificateAsync(new Brick.StorageAuthority.RevokeCert()
            {
                Id = id,
                RevocationReason = "Admin Action"
            });
            return Ok();
        }
    }

}
