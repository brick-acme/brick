﻿

using brickadmin.Shared;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;
using System.Linq;
using Grpc.Core;

namespace brickadmin.Server.Controllers
{
    [ApiController]
    [Route("api/orders")]
    public class OrdersController : ControllerBase
    {
        private ILogger logger;
        private Brick.StorageAuthority.StorageAuthority.StorageAuthorityClient client;
        public OrdersController(ILogger<OrdersController> l, Brick.StorageAuthority.StorageAuthority.StorageAuthorityClient c)
        {
            logger = l;
            client = c;
        }

        [HttpGet]
        [Route("{id}")]
        public async Task<ActionResult<Order>> GetOrder([FromRoute]string Id)
        {
            Brick.Core.Order order;
            try
            {
                order = await client.GetOrderAsync(new Brick.Core.IdRequest { Id = Id });
            }
            catch (RpcException e)
            {
                if (e.StatusCode == Grpc.Core.StatusCode.NotFound)
                {
                    return NotFound();
                }
                throw;
            }
            return Ok(new Order()
            {
                Id = order.Id,
                Status = order.Status,
                NotAfter = order.RequestedNotAfterDate,
                NotBefore = order.RequestedNotBeforeDate,
                Authz = order.Authz.Select(o => new Authorization()
                {
                    Id = o.Id,
                    Status = o.Status,
                    Type = o.Identifier.Type,
                    Value = o.Identifier.Value,
                    Challenges = o.Challenges.Select(c => new Shared.Challenge()
                    {
                        Status = c.Status,
                        Type = c.Type
                    }).ToList()
                }).ToList(),
                CertID = order.CertificateId
            });
        }
    }
}
