﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace brickadmin.Server.Controllers
{
    [ApiController]
    [Route("api/authz")]
    public class AuthorizationsController : ControllerBase
    {
        private ILogger logger;
        private Brick.StorageAuthority.StorageAuthority.StorageAuthorityClient client;

        public AuthorizationsController(ILogger<AuthorizationsController> l, Brick.StorageAuthority.StorageAuthority.StorageAuthorityClient c)
        {
            logger = l;
            client = c;
        }

        [HttpPost]
        [Route("{id}/authorize")]
        public async Task<IActionResult> AuthorizeAuthorization([FromRoute]string id)
        {
            var authz = await client.GetAuthorizationAsync(new Brick.Core.IdRequest() { Id = id });
            if (authz.Status != "pending")
            {
                return BadRequest(new
                {
                    message = "Authorization is not pending"
                });
            }
            var au = await client.AddChallengeAsync(new Brick.StorageAuthority.addChallengeToAuthz()
            {
                Id = id,
                Challenge = new Brick.Core.Challenge()
                {
                    Type = "manual-01",
                    Token = "",
                    Status = "valid",
                    Validated = DateTime.UtcNow.ToString("yyyy-MM-dd'T'HH:mm:ssZ")
                }
            });
            if (au.Status != "valid")
            {
                throw new Exception("TODO");
            }
            return Ok();
        }

    }
}
