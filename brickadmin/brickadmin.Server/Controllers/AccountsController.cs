﻿using brickadmin.Shared;
using Grpc.Core;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using OpenTracing;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace brickadmin.Server.Controllers
{
    [ApiController]
    [Route("api/accounts")]
    public class AccountsController : ControllerBase
    {
        private ILogger logger;
        private Brick.StorageAuthority.StorageAuthority.StorageAuthorityClient client;
        private ITracer tracer;
        public AccountsController(ILogger<AccountsController> l, Brick.StorageAuthority.StorageAuthority.StorageAuthorityClient c, ITracer t)
        {
            logger = l;
            client = c;
            tracer = t;
        }

        [Route("")]
        [HttpGet()]
        public async Task<ActionResult<IEnumerable<ShortAccount>>> GetAccounts()
        {
            var shortAccs = new List<ShortAccount>();
            using (var call = client.GetAllAccounts(new Brick.Core.Empty()))
            {
                while (await call.ResponseStream.MoveNext())
                {
                    shortAccs.Add(new ShortAccount()
                    {
                        ID = call.ResponseStream.Current.Id,
                        ExternalBinding = call.ResponseStream.Current.ExternalIdentifier ?? ""
                    });
                }
            }
            return Ok(shortAccs);
        }
        [Route("{id}")]
        [HttpGet]
        public async Task<ActionResult<Account>> GetAccount([FromRoute]string id)
        {
            //TODO: Split grpc calls into tasks
            try
            {
                var coreAccTask = client.GetAccountAsync(new Brick.Core.IdRequest() { Id = id });
                var ordersList = new List<string>();
                var authzList = new List<Authorization>();
                using (var scope = tracer.BuildSpan("getOrders").AsChildOf(tracer.ActiveSpan).StartActive(finishSpanOnDispose: true))
                using (var call = client.GetAccountOrderIDs(new Brick.Core.IdRequest() { Id = id }))
                {
                    while (await call.ResponseStream.MoveNext())
                        ordersList.Add(call.ResponseStream.Current.Id);
                }
                using (var scope = tracer.BuildSpan("getAuthorizations").AsChildOf(tracer.ActiveSpan).StartActive(finishSpanOnDispose: true))
                using (var call = client.GetAuthorizationsForAccount(new Brick.Core.IdRequest() { Id = id }))
                {
                    while (await call.ResponseStream.MoveNext())
                    {
                        authzList.Add(new Authorization()
                        {
                            Id = call.ResponseStream.Current.Id,
                            Status = call.ResponseStream.Current.Status,
                            Type = call.ResponseStream.Current.Identifier.Type,
                            Value = call.ResponseStream.Current.Identifier.Value,
                            Challenges = call.ResponseStream.Current.Challenges.Select(c => new Shared.Challenge()
                            {
                                Status = c.Status,
                                Type = c.Type
                            }).ToList()
                        });
                    }
                }
                var coreAcc = await coreAccTask;
                return Ok(new Account()
                {
                    Id = id,
                    Status = coreAcc.Status,
                    Orders = ordersList,
                    Authorizations = authzList
                });
            }
            catch (RpcException e)
            {
                if (e.StatusCode == Grpc.Core.StatusCode.NotFound)
                {
                    return NotFound();
                }
            }
            return StatusCode(500);
        }
    }
}
