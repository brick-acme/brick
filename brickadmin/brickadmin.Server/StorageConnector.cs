﻿using System;
using Grpc.Core;
using Grpc.Core.Interceptors;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using OpenTracing;
using OpenTracing.Contrib.Grpc.Interceptors;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;

namespace brickadmin.Server
{
    public class StorageOptions
    {
        public string connection { get; set; }
    }

    public static class ServiceCollectionExtensions
    {
        public static void AddStorageAuthority(this IServiceCollection services)
        {
            if (services is null)
                throw new ArgumentNullException(nameof(services));

            services.AddTransient<Brick.StorageAuthority.StorageAuthority.StorageAuthorityClient>(s =>
            {
                var tracer = s.GetRequiredService<ITracer>();
                var options = s.GetRequiredService<IOptions<StorageOptions>>();
                Channel channel = new Channel(options.Value.connection, ChannelCredentials.Insecure);
                ClientTracingInterceptor tracingInterceptor = new ClientTracingInterceptor(tracer);
                s.GetRequiredService<ILogger<StorageOptions>>().LogInformation($"Connecting to {options.Value.connection}");
                return new Brick.StorageAuthority.StorageAuthority.StorageAuthorityClient(channel.Intercept(tracingInterceptor));
            });
        }
    }
}
