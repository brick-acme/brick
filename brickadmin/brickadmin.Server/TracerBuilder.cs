﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using OpenTracing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace brickadmin.Server
{
    public static class TracerBuilder
    {
        public static void AddJaeger(this IServiceCollection services)
        {
            services.AddSingleton<ITracer>(provider =>
            {
                Environment.SetEnvironmentVariable("JAEGER_SERVICE_NAME", "brickadmin");
                var conf = Jaeger.Configuration.FromEnv(provider.GetRequiredService<ILoggerFactory>());
                var t = conf.GetTracerBuilder().WithSampler(new Jaeger.Samplers.ConstSampler(true)).Build();
                OpenTracing.Util.GlobalTracer.Register(t);
                return t;
            });
        }
    }
}
