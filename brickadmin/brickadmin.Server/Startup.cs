﻿using Grpc.Core;
using Microsoft.AspNetCore.Blazor.Server;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.ResponseCompression;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json.Serialization;
using System.Linq;
using System.Net.Mime;
using OpenTracing;
using OpenTracing.Util;
using Jaeger;
using Jaeger.Samplers;
using Jaeger.Reporters;
using OpenTracing.Contrib.Grpc.Interceptors;
using Grpc.Core.Interceptors;
using Microsoft.Extensions.Configuration;

namespace brickadmin.Server
{
    public class Startup
    {
        IConfiguration Configuration { get; set; }
        public Startup(IConfiguration config)
        {
            Configuration = config;
        }
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc();
            services.AddOptions();
            services.Configure<StorageOptions>(Configuration.GetSection("storage"));
            services.AddResponseCompression(options =>
            {
                options.MimeTypes = ResponseCompressionDefaults.MimeTypes.Concat(new[]
                {
                    MediaTypeNames.Application.Octet,
                    WasmMediaTypeNames.Application.Wasm,
                });
            });


            var sampler = new ProbabilisticSampler(samplingRate: 0.5);
            var remoteRep = new RemoteReporter.Builder().Build();

            services.AddJaeger();

            services.AddOpenTracing();
            services.AddStorageAuthority();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseResponseCompression();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseMvc();

            app.UseBlazor<Client.Program>();
        }
    }
}
