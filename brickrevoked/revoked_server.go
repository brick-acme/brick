/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

package main

import (
	"brick/brickrevoked/crl"
	"brick/brickrevoked/ocsp"
	"brick/config"
	"flag"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"os"

	"github.com/prometheus/client_golang/prometheus"

	_ "brick/grpc/resolvers/consul"

	opentracing "github.com/opentracing/opentracing-go"
	"github.com/sirupsen/logrus"
	jaegerconf "github.com/uber/jaeger-client-go/config"
)

var (
	configFile = flag.String("config", "./run/default-config.json", "Path to config file")
)

func main() {
	flag.Parse()
	mainL := config.MakeStandardLogger("main", false)
	mainL.Info("Starting up")
	var c config.BrickRevokedConfig
	config.ReadJSON(*configFile, &c)
	//Check if we should do runtime tracing
	if c.ProcessTracing {
		mainL.Warn("Doing Process-Level Tracing - DISABLE IF NOT TESTING")
		c := config.StartProcessTracing()
		defer c.Close()
	}
	//Check if we should do openTracing
	closer, err := config.SetupOpenTracing(c.Opentracing, "brickrevoked", config.MakeStandardLogger("opentracing", true), prometheus.DefaultRegisterer)
	if err != nil {
		panic(err)
	}
	defer closer.Close()

	connector := new(c.CA.Address, c.Storage.Address)
	ocspHandler := ocsp.GetHandler(connector.store, connector.ca)
	crlHandler := crl.GetHandler(connector)
	http.Handle("/crl", crlHandler)
	http.Handle("/", ocspHandler)
	mainL.Infof("Listening on Port %d", c.Port)
	mainL.Info(http.ListenAndServe(fmt.Sprintf(":%d", c.Port), nil))
}

//SetupOpenTracing sets up Opentracing (enabled with jaeger, or disabled with NoopTracer)
func SetupOpenTracing(c config.OpentracingConfig, logger logrus.FieldLogger) (io.Closer, error) {
	if !c.Enable {
		opentracing.SetGlobalTracer(opentracing.NoopTracer{})
		return ioutil.NopCloser(os.Stdin), nil // ugliest hack in the history of hacks
	}
	logger.Info("Enabling Opentracing Jaeger Instrumentation")
	cfg := &jaegerconf.Configuration{
		Sampler: &jaegerconf.SamplerConfig{
			Type:  "const",
			Param: 1,
		},
		Reporter: &jaegerconf.ReporterConfig{
			LogSpans: false,
		},
	}
	tracer, closer, err := cfg.New("brickrevoked", jaegerconf.Logger(&jaegerLogger{logger: logger}))
	if err != nil {
		panic(fmt.Sprintf("Could not init Jaeger: %s", err))
	}
	opentracing.SetGlobalTracer(tracer)
	return closer, nil
}

type jaegerLogger struct {
	logger logrus.FieldLogger
}

func (l *jaegerLogger) Error(msg string) {
	l.logger.Error(msg)
}
func (l *jaegerLogger) Infof(msg string, args ...interface{}) {
	l.logger.Infof(msg, args...)
}
