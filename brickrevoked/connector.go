/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

package main

import (
	"brick/config"
	"brick/core"
	"brick/core/berrors"
	"brick/grpc"
	"context"
	"math/big"

	opentracing "github.com/opentracing/opentracing-go"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type storageWrapper interface {
	GetCRL(context.Context, string) ([]byte, error)
	StoreCRL(context.Context, string, []byte) error
	GetCertificateBySerial(context.Context, *big.Int, []byte) (*core.Certificate, error)
}

type caWrapper interface {
	GenerateCRL(context.Context, string) ([]byte, error)
	GenerateOCSP(context.Context, *core.Certificate, []byte) ([]byte, error)
}

type revocationConnector struct {
	ca    caWrapper
	store storageWrapper
}

func new(caAddr string, storageAddr string) *revocationConnector {
	r := &revocationConnector{}
	var err error
	r.store, err = grpc.NewStorageWrapper(storageAddr, config.MakeStandardLogger("revoke-SA", false))
	if err != nil {
		panic(err)
	}
	r.ca, err = grpc.NewCAWrapper(caAddr, config.MakeStandardLogger("revoke-CA", false))
	if err != nil {
		panic(err)
	}
	return r
}
func (r *revocationConnector) GetCRLFromStorage(ctx context.Context, castring string) ([]byte, error) {
	span, ctx := opentracing.StartSpanFromContext(ctx, "GetCRLFromStorage")
	defer span.Finish()
	span.SetTag("ca-name", castring)
	crl, err := r.store.GetCRL(ctx, castring)
	if err != nil {
		if status.Code(err) == codes.NotFound {
			return nil, berrors.NotFoundError("CRL does not exist in store")
		}
		return nil, err
	}
	return crl, nil
}
func (r *revocationConnector) GetCRLFromCA(ctx context.Context, caname string) ([]byte, error) {
	span, ctx := opentracing.StartSpanFromContext(ctx, "GetCRLFromCA")
	defer span.Finish()
	return r.ca.GenerateCRL(ctx, caname)
}
func (r *revocationConnector) StoreCRLInStorage(ctx context.Context, caname string, crl []byte) error {
	span, ctx := opentracing.StartSpanFromContext(ctx, "StoreCRLInStorage")
	defer span.Finish()
	err := r.store.StoreCRL(ctx, caname, crl)
	return err
}
