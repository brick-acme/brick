/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

package crl

import (
	"brick/core/berrors"
	"context"
	"crypto/x509"
	"fmt"
	"net/http"
	"time"

	"github.com/opentracing/opentracing-go"
)

type connector interface {
	GetCRLFromStorage(context.Context, string) ([]byte, error)
	GetCRLFromCA(context.Context, string) ([]byte, error)
	StoreCRLInStorage(context.Context, string, []byte) error
}

var client connector

//GetHandler returns the Handler associated with retrieving the
func GetHandler(c connector) http.Handler {
	initialize(c)
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx, cancel := context.WithTimeout(context.Background(), time.Second*2)
		defer cancel()
		span, ctx := opentracing.StartSpanFromContext(ctx, "HandleCRL")
		defer span.Finish()
		if r.Method != "GET" {
			spanError(span, fmt.Errorf("Method %s not allowed", r.Method))
			w.WriteHeader(http.StatusMethodNotAllowed)
			return
		}
		crlBytes, err := client.GetCRLFromStorage(ctx, "")
		if err != nil {
			spanError(span, err)
			if _, ok := berrors.IsNotFoundError(err); ok {
				crlBytes, err = getCRLFromCAAndUpdate(ctx)
				if err != nil {
					w.WriteHeader(http.StatusInternalServerError)
					return
				}
			} else {
				spanError(span, err)
				w.WriteHeader(http.StatusInternalServerError)
				return
			}
		}
		middleSpan := opentracing.StartSpan("ParseDERCRL", opentracing.ChildOf(span.Context()))
		crl, err := x509.ParseDERCRL(crlBytes)
		if err != nil {
			spanError(span, err)
			w.WriteHeader(http.StatusInternalServerError)
			middleSpan.Finish()
			return
		}
		middleSpan.Finish()
		if crl.TBSCertList.NextUpdate.UTC().Before(time.Now().UTC()) {
			crlBytes, err = getCRLFromCAAndUpdate(ctx)
			if err != nil {
				spanError(span, err)
				w.WriteHeader(500)
				return
			}
		}
		w.Header().Set("content-type", "application/pkix-crl")
		w.WriteHeader(200)
		w.Write(crlBytes)
		return
	})
}

func initialize(c connector) {
	client = c
}

func spanError(span opentracing.Span, err error) error {
	span.SetTag("error", true)
	span.LogKV("event", "error", "error.message", err.Error(), "error.object", err)
	return err
}

func getCRLFromCAAndUpdate(ctx context.Context) ([]byte, error) {
	span, ctx := opentracing.StartSpanFromContext(ctx, "getCRLFromCAAndUpdate")
	defer span.Finish()
	crlBytes, err := client.GetCRLFromCA(ctx, "")
	if err != nil {
		return nil, spanError(span, err)
	}
	err = client.StoreCRLInStorage(ctx, "", crlBytes)
	if err != nil {
		return nil, spanError(span, err)
	}
	return crlBytes, err
}
