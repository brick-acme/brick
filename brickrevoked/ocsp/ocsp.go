/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

package ocsp

import (
	cacommon "brick/brickca/ca"
	"brick/brickrevoked/ocsp/internal"
	"brick/core"
	"brick/core/berrors"
	"bytes"
	"context"
	"encoding/base64"
	"encoding/hex"
	"errors"
	"fmt"
	"io"
	"math/big"
	"net/http"

	opentracing "github.com/opentracing/opentracing-go"
)

var (
	StrictMode bool
)

const (
	ocspContentType = "application/ocsp-request"
)

type storageI interface {
	GetCertificateBySerial(context.Context, *big.Int, []byte) (*core.Certificate, error)
}
type caI interface {
	GenerateOCSP(ctx context.Context, cert *core.Certificate, nonce []byte) ([]byte, error)
}

func GetHandler(storage storageI, ca caI) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()
		span, ctx := opentracing.StartSpanFromContext(ctx, "HandleOCSP")
		defer span.Finish()
		if StrictMode && r.Header.Get("Content-Type") != ocspContentType {
			handleError(fmt.Errorf("Strict Mode requires content-Type %s", ocspContentType), w, span)
			return
		}
		b := &bytes.Buffer{}
		switch r.Method {
		case "POST":
			bodyReader := io.LimitReader(r.Body, 100000) //Expect an ocsp request to be smaller than 100k
			_, err := io.Copy(b, bodyReader)
			if err != nil {
				r.Body.Close()
				handleError(err, w, span)
				return
			}
			r.Body.Close()
		case "GET":
			gd, err := base64.StdEncoding.DecodeString(r.URL.Path[1:])
			if err != nil {
				handleError(err, w, span)
				return
			}
			r := bytes.NewReader(gd)
			b.ReadFrom(r)
		default:
			handleError(fmt.Errorf("Unsupported Request-Method %s", r.Method), w, span)
			return
		}
		request, exts, err := internal.ParseRequest(b.Bytes())
		if err != nil {
			handleError(err, w, span)
		}
		var nonce = []byte{}
		//Find the Nonce extension
		for _, ext := range exts {
			if ext.Id.Equal(cacommon.OidNonce) {
				nonce = ext.Value
				span.SetTag("nonce", hex.EncodeToString(nonce))
			} else {
				if ext.Critical == true {
					span.LogKV("event", "error", "error.message", "Unknown Critical Extension", "ext.oid", ext.Id.String())
					handleError(errors.New("Unknown Nonce Extensions"), w, span)
					return
				}
			}
		}
		cert, err := storage.GetCertificateBySerial(ctx, request.SerialNumber, request.IssuerNameHash)
		if err != nil {
			if _, ok := berrors.IsNotFoundError(err); ok == true {
				w.WriteHeader(http.StatusNotFound)
				return
			}
			handleError(err, w, span)
			return
		}
		ocspResp, err := ca.GenerateOCSP(ctx, cert, nonce)
		if err != nil {
			if _, ok := berrors.IsNotFoundError(err); ok == true {
				w.WriteHeader(http.StatusNotFound)
				return
			}
			handleError(err, w, span)
			return
		}
		w.Write(ocspResp)
		return
	})
}

func handleError(err error, w http.ResponseWriter, span opentracing.Span) {
	span.SetTag("error", "true")
	span.LogKV("event", "error", "error.object", err)
	w.WriteHeader(http.StatusInternalServerError)
}

func verify(ctx context.Context, rawreq []byte) ([]byte, error) {
	span, ctx := opentracing.StartSpanFromContext(ctx, "verify")
	defer span.Finish()
	req, exts, err := internal.ParseRequest(rawreq)
	if err != nil {
		return nil, err
	}
	panic("NYI")
	fmt.Print(req, exts, err, ctx)
	return nil, nil
}
