# Brick

## What is this?

If you have never heard of ACME or the Let's Encrypt initiative, consider reading up on it first:
 - (ACME)[https://datatracker.ietf.org/doc/draft-ietf-acme-acme/]
 - (Let's Encrypt)[https://letsencrypt.org/]

Brick is an implementation of the ACME-protocal as defined in the RFC draft - which will (hopefully) be fully standardized soon.

It is modeled after the (implementation)[https://github.com/letsencrypt/boulder] used, developed and maintained by the Let's Encrypt team, as well as the alternate,
testing (version)[https://github.com/letsencrypt/pebble]. That work is also the inspiration for the name *Brick* - it's a solution which lies in between *Boulder* and *Pebble* in scope, and is manufactured more towards an in-house, *enterprisey* enivronment.

## Why is this?

A company-internal network of certain scale will at some point have to deal with issues of authenticating & encrypting network connections.
These may be for databases, microservices, or existing commercial or open-source systems. For this purpose, an internal PKI/CA will have to be built or bought.

Nowadays, as virtually every enterprise is moving to Microservice architectures, the time necessary to set up a new system needs to be reduced, as it hampers iteration speed.
This is why ordering in-house certificates via forms, manual validation, and manual processes is no longer sufficient - enter ACME. Since about ietf-draft-7, this protocol is *production-ready* for internal certificate management, but a good software implementation is missing - at least if you consider the massive operational challenges of installing and maintaining **Boulder**.

So just roll your own.

