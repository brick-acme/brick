/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

package main

import (
	"brick/brickweb/external"
	"brick/brickweb/va"
	"brick/brickweb/wfe/nonce"
	"context"
	"flag"
	"fmt"
	"net/http"
	"os"
	"os/signal"

	"brick/brickweb/wfe"
	"brick/config"
	"brick/core"
	"brick/grpc"
	_ "brick/grpc/resolvers/consul"

	"github.com/prometheus/client_golang/prometheus"
)

var (
	configFile = flag.String("config", "./run/default-config.json", "The Config File (JSON)")
)

func main() {
	mainLogger := config.MakeStandardLogger("brickweb", false)
	mainLogger.Info("Starting up")
	flag.Parse()
	var c = config.BrickWebConfig{}
	err := config.ReadJSON(*configFile, &c)
	if err != nil {
		mainLogger.Panic(err)
	}
	//Check if we should do runtime tracing
	if c.ProcessTracing {
		mainLogger.Warn("Doing Process-Level Tracing - DISABLE IF NOT TESTING")
		c := config.StartProcessTracing()
		c.Close()
	}

	//Check if we should do openTracing
	closer, err := config.SetupOpenTracing(c.Opentracing, "brickweb", config.MakeStandardLogger("opentracing", false), prometheus.DefaultRegisterer)
	if err != nil {
		panic(err)
	}
	defer closer.Close()

	//CaWrapper
	ca, err := grpc.NewCAWrapper(c.CA.Address, config.MakeStandardLogger("ca", true))
	storage, err := grpc.NewStorageWrapper(c.Storage.Address, config.MakeStandardLogger("storage", c.JSONLogging))
	verificationChan := make(chan core.VerificationRequest, 10)
	verifyConfig(&c)
	verificationAuthority := va.New(verificationChan, config.MakeStandardLogger("va", c.JSONLogging), storage)
	wfe := wfe.New(config.MakeStandardLogger("wfe", c.JSONLogging), ca, storage, verificationChan)

	//Initialize Nonce Subsystem
	switch c.Nonce.Provider {
	case "redis":
		mainLogger.WithField("address", c.Nonce.RedisAddr).Infof("Using Redis Provider")
		noncer := nonce.NewRedisNoncer(c.Nonce.RedisAddr)
		wfe.Noncer = noncer
	default:
		wfe.Noncer = nonce.NewNoncer()
	}
	//Initialize HTTP Server
	srv := &http.Server{
		Addr:    fmt.Sprintf(":%d", c.Port),
		Handler: wfe.Handler(),
	}
	if c.AccountCreation.RequireExternal {
		wfe.AccountValidator = external.GetValidator(c.AccountCreation.ValidatorName, c.AccountCreation.ValidatorConfig)
	} else {
		wfe.AccountValidator = nil
	}
	go verificationAuthority.Start()
	wfe.BasePath = c.BaseURL
	//"Gracefully" exit on ctrl-c or docker stop
	go catchSIGINT(srv)
	//start server
	mainLogger.Infof("Starting listening on %s", srv.Addr)
	if c.Web.TLS {
		mainLogger.Warn("Running with TLS enabled is not recommended; try using a reverse proxy instead")
		srv.Addr = ":443"
		err = srv.ListenAndServeTLS(c.Web.CertFile, c.Web.KeyFile)
	} else {
		err = srv.ListenAndServe()
	}
	mainLogger.WithError(err).Warning("Exit")
}

func catchSIGINT(srv *http.Server) {
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)
	for range c {
		srv.Shutdown(context.Background())
	}
}

type cacertLister interface {
	GetAvailableCaCertificates(rootCtx context.Context) ([]*core.CaCertificate, error)
}

func verifyConfig(c *config.BrickWebConfig) {
	if c.BaseURL == "" {
		panic("BrickWeb BasePath cannot be unset (base_url)")
	}
	if c.Port == 0 {
		c.Port = 80
	}
}
