/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

package wfe

import (
	"net/http"

	"brick/brickweb/acme"
	"brick/core/berrors"

	opentracing "github.com/opentracing/opentracing-go"
)

func SpanError(span opentracing.Span, err error) {
	span.SetTag("error", true)
	span.LogKV("event", "error", "error.object", err)
}

func (wfe *WebFrontEndImpl) sendError(prob *acme.ProblemDetails, response http.ResponseWriter) {
	if prob == nil {
		wfe.logger.Error("OUCH! Problem document in sendError is nil!")
		response.WriteHeader(http.StatusInternalServerError)
		return
	}
	problemDoc, err := marshalIndent(prob)
	if err != nil {
		problemDoc = []byte("{\"detail\": \"Problem marshalling error message.\"}")
	}

	response.Header().Set("Content-Type", "application/problem+json")
	response.WriteHeader(prob.HTTPStatus)
	response.Write(problemDoc)
	wfe.logger.WithError(prob).Info("Returned Problem")
}

//handleError handles the generic Error err, checking whether it is an acme.ProblemDetails, a berrors.BrickProblem or an unknown Error
//generally call it and return in http handler methods
func (wfe *WebFrontEndImpl) handleError(err error, response http.ResponseWriter, span opentracing.Span) {
	switch err.(type) {
	case *acme.ProblemDetails:
		wfe.sendError(err.(*acme.ProblemDetails), response)
		return
	case *berrors.BrickProblem:
		wfe.sendError(acme.InternalErrorProblem("Something went wrong internally"), response)
		SpanError(span, err)
		wfe.logger.WithError(err).Error("An Error has occured")
		return
	default:
		wfe.logger.WithError(err).Error("An unexpected Error has occured")
		SpanError(span, err)
		wfe.sendError(acme.InternalErrorProblem("Something went very wrong internally"), response)
		return
	}
}
