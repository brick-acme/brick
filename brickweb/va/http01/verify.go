/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package http01

import (
	"brick/brickweb/acme"
	"brick/brickweb/metrics"
	"brick/core"
	"context"
	"crypto/tls"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"net/url"
	"runtime"
	"runtime/debug"
	"strings"
	"time"

	opentracing "github.com/opentracing/opentracing-go"
)

func userAgent() string {
	return fmt.Sprintf("Brick (%s, %s)", runtime.GOOS, runtime.GOARCH)
}

func VerifyHTTP01(ctx context.Context, req core.VerificationRequest) error {
	span, ctx := opentracing.StartSpanFromContext(ctx, "VerifyHTTP01")
	defer span.Finish()
	body, err := fetchToken(ctx, req.Authorization.Identifier.Value, req.Challenge.Token)
	if err != nil {
		return convertToAcmeProblem(span, err)
	}
	expectedKeyAuthorization, err := acme.ExpectedKeyAuthorization(ctx, req.Challenge.Token, req.AccountJWK)
	if err != nil {
		return convertToAcmeProblem(span, err)
	}
	payload := strings.TrimRight(string(body), "\n\r\t")
	if payload != expectedKeyAuthorization {
		return acme.UnauthorizedProblem(fmt.Sprintf("The key authorization file from server did not match this challenge: %q != %q", expectedKeyAuthorization, payload))
	}
	return nil
}

func fetchToken(ctx context.Context, identifier string, token string) ([]byte, error) {
	span, ctx := opentracing.StartSpanFromContext(ctx, "fetchToken")
	defer span.Finish()
	url := &url.URL{
		Scheme: "http",
		Host:   fmt.Sprintf("%s:%s", identifier, "80"), //Challenge Validation hardcoded to port 80. TODO: Investigate other ports
		Path:   fmt.Sprintf("%s%s", acme.HTTP01BaseURL, token),
	}
	span.SetTag("url", url.String())
	httpRequest, err := http.NewRequest(http.MethodGet, url.String(), nil)
	if err != nil {
		span.LogKV("event", "error", "error.object", err, "message", "Could not construct HTTP-Request")
		return nil, err
	}
	httpRequest.Header.Set("user-agent", userAgent())
	httpRequest.Header.Set("accept", "*/*")
	httpRequest.Header.Set("x-random-header-value", "abc")
	transport := &http.Transport{
		DisableKeepAlives: true,                                                   //we only do one roundtrip, ever
		TLSNextProto:      map[string]func(string, *tls.Conn) http.RoundTripper{}, //As per https://godoc.org/net/http this causes no upgrades to http/2
	}
	client := &http.Client{
		Transport: transport,
		Timeout:   time.Second * 2,
	}
	resp, err := client.Do(httpRequest)
	if err != nil {
		span.LogKV("event", "warning", "error.object", err)
		metrics.ChallengeHttpRequests.WithLabelValues("0").Inc()
		return nil, acme.ConnectionProblem(fmt.Sprintf("Could not connect to url %s", url.String()))
	}
	span.LogKV("message", "Received Http Response from ChallengeURL", "status", resp.Status)
	span.SetTag("http.status_code", resp.StatusCode)
	metrics.ChallengeHttpRequests.WithLabelValues(fmt.Sprintf("%d", resp.StatusCode)).Inc()
	if resp.StatusCode != http.StatusOK {
		//ACME RFC mandates status 200 on challenge GET
		span.LogKV("event", "auth", "authorization", "failed", "message", "Non-200 status code from GET")
		return nil, acme.UnauthorizedProblem(fmt.Sprintf("Non-200 status code from GET %s: %d", url.String(), resp.StatusCode))
	}
	defer resp.Body.Close()
	bodyReader := io.LimitReader(resp.Body, 1000)
	body, _ := ioutil.ReadAll(bodyReader) // I don't believe there is an error that could happen here
	return body, nil
}

func convertToAcmeProblem(span opentracing.Span, err error) *acme.ProblemDetails {
	prob, ok := err.(*acme.ProblemDetails)
	if ok {
		return prob
	}
	span.SetTag("error", true)
	span.LogKV("event", "error", "error.object", err, "stack", string(debug.Stack()))
	return acme.InternalErrorProblem("An Internal Error happened")
}
