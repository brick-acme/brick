/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

package va

import (
	"brick/brickweb/acme"
	"brick/brickweb/db"
	"brick/brickweb/va/http01"
	"brick/core"
	"context"
	"fmt"
	"math/rand"
	"time"

	"github.com/sirupsen/logrus"

	opentracing "github.com/opentracing/opentracing-go"
)

const maxChallengeRetries = 5

var (
	verificationFuncs map[string]func(context.Context, core.VerificationRequest) error
)

type storage interface {
	UpdateAuthorization(context.Context, *core.Challenge, string, string) error
}

//VA is the interface used for a VerificationAuthority
type VA interface {
	Start()
}

func New(ch chan core.VerificationRequest, l logrus.FieldLogger, s storage) VA {
	return &vaImpl{
		logger: l,
		c:      ch,
		db:     s,
	}
}

func (va *vaImpl) Start() {
	va.startLoop()
}

func init() {
	verificationFuncs = make(map[string]func(context.Context, core.VerificationRequest) error)
	verificationFuncs["http-01"] = http01.VerifyHTTP01
}

type vaImpl struct {
	c      chan core.VerificationRequest
	logger logrus.FieldLogger
	db     storage
}

func (va *vaImpl) startLoop() {
	va.logger.Info("VA starting")
	go func() {
		for req := range va.c {
			go va.performValidation(req)
		}
	}()
}

func (va *vaImpl) performValidation(req core.VerificationRequest) {
	prevSpan := opentracing.SpanFromContext(req.Context)
	span := opentracing.StartSpan("PerformValidation", opentracing.FollowsFrom(prevSpan.Context()))
	span.SetTag("retry-nr", req.Retries)
	ctx := opentracing.ContextWithSpan(context.Background(), span)
	defer recoverAndLogError(span) //This is done to prevent a complete crash of the application in case of faulty challenge validation code
	defer span.Finish()
	chal := req.Challenge
	verFunc := verificationFuncs[chal.Type]
	//Sleep here for a random few seconds
	time.Sleep(time.Second * time.Duration(rand.Intn(10)+1))
	if verFunc == nil {
		span.LogKV("event", "error", "error.message", fmt.Sprintf("%s Verification method not found", chal.Type))
		return
	}
	err := verFunc(ctx, req)
	if err != nil {
		if req.Retries < maxChallengeRetries {
			req.Retries++
			span.SetTag("error", true)
			span.LogKV("event", "error", "error.message", "Retrying Challenge Validation", "error.RetryCount", req.Retries)
			va.c <- req
			return
		}
		e := db.SetChallengeInvalid(ctx, va.db, req.Challenge, req.Authorization, getAcmeProblem(span, err))
		if e != nil {
			span.SetTag("error", true)
			span.SetTag("critical", true)
			span.LogKV("event", "error", "error.kind", "critical", "error.object", e)
			return
		}
		return
	}
	err = db.SetChallengeValid(ctx, va.db, req.Challenge, req.Authorization)
	if err != nil {
		span.SetTag("error", true)
		span.SetTag("critical", true)
		span.LogKV("event", "error", "error.kind", "critical", "error.object", err)
	}
	return
}

func recoverAndLogError(span opentracing.Span) {
	x := recover()
	if x != nil {
		span.LogKV("event", "error", "error.kind", "panic", "error.object", x)
	}
}

func getAcmeProblem(span opentracing.Span, err error) *acme.ProblemDetails {
	prob, ok := err.(*acme.ProblemDetails)
	if ok {
		return prob
	}
	span.LogKV("event", "error", "error.object", err, "message", "An Error occured while verifying")
	span.SetTag("error", true)
	return acme.InternalErrorProblem("An internal Error occured while verifying")
}
