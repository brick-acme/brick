/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

package grpc

import (
	"brick/core/berrors"

	opentracing "github.com/opentracing/opentracing-go"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

var callOptions = []grpc.CallOption{grpc.FailFast(false)}

func (s *storage) handleError(span opentracing.Span, err error) error {
	status := status.Code(err)
	if status == codes.DeadlineExceeded {
		return berrors.TimeoutError()
	} else if status == codes.NotFound {
		return berrors.NotFoundError("The object does not exist")
	} else {
		span.SetTag("error", true)
		span.LogKV("event", "error", "error.object", err)
		return berrors.UnknownError(err)
	}
}

func handleError(span opentracing.Span, err error) error {
	if err == nil {
		return nil
	}
	span.SetTag("error", true)
	span.LogKV("event", "error", "error.object", err)
	status := status.Code(err)
	if status == codes.DeadlineExceeded {
		return berrors.TimeoutError()
	} else if status == codes.NotFound {
		return berrors.NotFoundError("The object does not exist")
	} else {
		return berrors.UnknownError(err)
	}
}
