cd ./corepb
go generate
cd ../sapb
go generate
cd ../capb
go generate
cd ..
protoc.exe -I . --csharp_out corepb/ --grpc_out corepb/ corepb/core.proto --plugin=protoc-gen-grpc=packages\grpc.tools\1.16.0\tools\windows_x64\grpc_csharp_plugin.exe
protoc.exe -I . --csharp_out sapb/ --grpc_out sapb/ sapb/sa.proto --plugin=protoc-gen-grpc=packages\grpc.tools\1.16.0\tools\windows_x64\grpc_csharp_plugin.exe
dotnet build -c Release --no-incremental
dotnet pack -c Release -o "."