﻿using OpenTracing;
using System.Collections.Generic;

namespace OpenTracing.Extensions
{
    public static class SpanExtensions
    {
        public static void LogDBStatement(this ISpan span, string DBInstance, string DBStatement, string DBType, string DBUser = null)
        {
            span.Log(new Dictionary<string, object>()
                {
                    {"db.instance", "brick" },
                    {"db.statement", DBStatement},
                    {"db.Type", DBType }
                });
        }
    }
}
