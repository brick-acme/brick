﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace brickstorage.Integrations
{
    public static class Consul
    {
        public static async Task RegisterWithConsul(int Port = 9000, string ServiceName = "brickstorage")
        {
            using (var http = new HttpClient())
            {
                var stringContent = new StringContent(JsonConvert.SerializeObject(new
                {
                    ID = ServiceName,
                    Name = ServiceName,
                    Port = Port,
                    Check = new
                    {
                        Name = $"Grpc-{ServiceName}-Check",
                        ID = ServiceName,
                        grpc = $"127.0.0.1:{Port}",
                        grpc_use_tls = false,
                        interval = "5s"
                    }
                }));
                var consulAddr = Environment.GetEnvironmentVariable("CONSUL_HTTP_ADDR");
                var defaultConsulAddr = "http://localhost:8500";
                consulAddr = consulAddr ?? defaultConsulAddr;
                await http.PutAsync($"{consulAddr}/v1/agent/service/register", stringContent);
            }
        }
    }
}
