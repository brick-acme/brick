﻿using Brick.Core;
using Grpc.Core;
using Grpc.Health.V1;
using Microsoft.Extensions.Logging;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using static Grpc.Health.V1.HealthCheckResponse.Types;

namespace brickstorage
{
    class HealthCheckImpl : Grpc.Health.V1.Health.HealthBase
    {
        private IMongoDatabase mongo;
        private ILogger Log;
        public HealthCheckImpl(IMongoDatabase mongo, ILogger logger)
        {
            this.mongo = mongo;
            Log = logger;
        }
        public async override Task<HealthCheckResponse> Check(HealthCheckRequest request, ServerCallContext context)
        {
            if (request.Service != "brickstorage" && request.Service != "")
            {
                throw new RpcException(new Status(StatusCode.NotFound, $"The Service {request.Service} does not exist"));
            }
            await mongo.RunCommandAsync((Command<BsonDocument>)"{ping:1}", cancellationToken: context.CancellationToken);
            return new HealthCheckResponse()
            {
                Status = ServingStatus.Serving
            };
        }
    }
}
