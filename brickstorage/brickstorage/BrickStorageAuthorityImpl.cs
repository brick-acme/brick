﻿using Brick.Core;
using Brick.StorageAuthority;
using EnsureThat;
using Grpc.Core;
using Microsoft.Extensions.Logging;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using OpenTracing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace brickstorage
{
    class BrickStorageAuthorityImpl : Brick.StorageAuthority.StorageAuthority.StorageAuthorityBase
    {
        private ILogger logger;
        private IMongoCollection<Model.Authorization> Authorizations;
        private IMongoCollection<Model.Account> Accounts;
        private IMongoCollection<Model.Order> Orders;
        private IMongoCollection<Model.Certificate> Certificates;
        private IMongoCollection<Model.CaCertificate> CaCertificates;
        private IMongoCollection<Model.CRL> CRLs;
        private ITracer tracer;
        public BrickStorageAuthorityImpl(IMongoDatabase db, ILogger logger, ITracer tracer)
        {
            var tokenSource = new CancellationTokenSource(4000);
            Authorizations = db.GetCollection<Model.Authorization>("authz");
            Accounts = db.GetCollection<Model.Account>("accounts");
            Orders = db.GetCollection<Model.Order>("orders");
            Certificates = db.GetCollection<Model.Certificate>("certificates");
            CaCertificates = db.GetCollection<Model.CaCertificate>("cacertificates");
            CRLs = db.GetCollection<Model.CRL>("crls");
            this.logger = logger;
            this.tracer = tracer;
            try
            {
                Model.Authorization.CreateIndices(Authorizations, tokenSource.Token).Wait();
                Model.Certificate.CreateIndices(Certificates, tokenSource.Token).Wait();
            }
            catch (AggregateException e) when (e.InnerExceptions.Any(ex => ex.GetType() == typeof(TaskCanceledException)))
            {
                logger.LogCritical("Apparently, MongoDB is unreachable");
                throw new Exception("Mongodb unreachable");
            }
        }
        public override async Task<Brick.Core.Account> GetAccount(IdRequest request, ServerCallContext context)
        {
            Model.Account acc;
            using (var scope = tracer.BuildSpan("mongo.SingleAccount(Id)").AsChildOf(tracer.ActiveSpan).WithTag("id", request.Id).StartActive(finishSpanOnDispose: true))
            {
                acc = await Accounts.AsQueryable().SingleOrDefaultAsync(a => a.Id == request.Id, cancellationToken: context.CancellationToken);
                if (acc == null)
                {
                    throw new RpcException(new Status(StatusCode.NotFound, "No such Account"));
                }
            }
            var keyBytes = Google.Protobuf.ByteString.CopyFrom(acc.Key);
            var returnAcct = new Brick.Core.Account()
            {
                Id = acc.Id.ToString(),
                Status = acc.Status,
                CreatedAt = acc.CreatedAt,
                Key = keyBytes,
                ExternalIdentifier = acc.ExternalIdentifier ?? ""
            };

            returnAcct.Contact.AddRange(acc.Contact);
            return returnAcct;
        }
        public async override Task<Empty> AddAccount(Account request, ServerCallContext context)
        {
            var newAcct = new Model.Account()
            {
                Id = request.Id,
                Status = request.Status,
                CreatedAt = request.CreatedAt,
                Key = request.Key.ToArray(),
                Contact = request.Contact.ToList(),
                ExternalIdentifier = request.ExternalIdentifier ?? ""
            };
            using (var scope = tracer.BuildSpan("mongo.AddSingleAccount()").AsChildOf(tracer.ActiveSpan).WithTag("id", request.Id).StartActive(finishSpanOnDispose: true))
            {
                await Accounts.InsertOneAsync(newAcct, cancellationToken: context.CancellationToken);
            }
            logger.LogInformation("Added one {Account}", request.Id);
            return new Empty();
        }
        public async override Task<Empty> UpdateAccount(Account request, ServerCallContext context)
        {
            using (var scope = tracer.BuildSpan("mongo.GetAccount()").AsChildOf(tracer.ActiveSpan).WithTag("id", request.Id).StartActive(finishSpanOnDispose: true))
            {
                var oldAcc = await Accounts.AsQueryable().SingleOrDefaultAsync(a => a.Id == request.Id);
                if (oldAcc == null)
                {
                    throw new RpcException(new Status(StatusCode.NotFound, "Account does not exist"));
                }
                //Ensure.That(System.DateTime.Parse(request.CreatedAt)).Is(oldAcc.CreatedAt);   IDK issues with UTC
            }
            using (var scope = tracer.BuildSpan("mongo.UpdateAccount()").AsChildOf(tracer.ActiveSpan).WithTag("id", request.Id).StartActive(finishSpanOnDispose: true))
            {
                var updateDef = Builders<Model.Account>.Update
                .Set(acc => acc.Contact, request.Contact.ToList())
                .Set(acc => acc.Key, request.Key.ToArray())
                .Set(acc => acc.Status, request.Status);
                //Cannot change CreatedAt!
                //Cannot change ExternalIdentifier
                await Accounts.UpdateOneAsync(a => a.Id == request.Id, updateDef);
            }
            return new Empty();
        }
        public override async Task<IdResponse> AddOrder(newOrder request, ServerCallContext context)
        {
            try
            {
                using (var scope = tracer.BuildSpan("Input Validation").AsChildOf(tracer.ActiveSpan).StartActive(finishSpanOnDispose: true))
                {
                    Ensure.That(request.AccountId).IsNotNullOrEmpty();
                    EnsureArg.HasItems(request.AuthzIDs);
                }
            }
            catch (ArgumentException e)
            {
                throw new RpcException(new Status(StatusCode.InvalidArgument, e.Message));
            }
            using (var scope = tracer.BuildSpan("mongo.AddOrder").AsChildOf(tracer.ActiveSpan).StartActive(finishSpanOnDispose: true))
            {
                Model.Order newOrder = new Model.Order()
                {
                    Status = "pending",  //TODO: Formalize Order Status - note that orders are always created in pending according to ACME-14 7.1.6
                    RequestedNotAfter = request.RequestedNotAfterDate,
                    RequestedNotBefore = request.RequestedNotBeforeDate,
                    AccountId = request.AccountId,
                    Expires = request.ExpiresDate
                };
                logger.LogInformation("Adding an Order", newOrder);
                newOrder.AuthzIDs = request.AuthzIDs.ToList();
                await Orders.InsertOneAsync(newOrder);
                return new IdResponse()
                {
                    Id = newOrder.ID.ToString()
                };
            }
        }

        public async override Task<Order> GetOrder(IdRequest request, ServerCallContext context)
        {
            using (var scope = tracer.BuildSpan("Input Validation").AsChildOf(tracer.ActiveSpan).StartActive(finishSpanOnDispose: true))
            {
                Ensure.That(request.Id).IsNotNullOrEmpty();
            }
            using (var scope = tracer.BuildSpan("mongo.FindOrder()").AsChildOf(tracer.ActiveSpan).WithTag("id", request.Id).StartActive(finishSpanOnDispose: true))
            {
                var order = await Orders.AsQueryable().SingleOrDefaultAsync(o => o.ID == request.Id);
                if (order == null)
                {
                    throw new RpcException(new Status(StatusCode.NotFound, "Order not found"));
                }
                var authzTasks = order.AuthzIDs.Select(async authzId =>
                {
                    using (tracer.BuildSpan("mongo.FindAuthz()").AsChildOf(tracer.ActiveSpan).WithTag("id", authzId).StartActive(finishSpanOnDispose: true))
                    {
                        return await Authorizations.AsQueryable().SingleOrDefaultAsync(au => au.Id == authzId);
                    }
                });
                var authz = await Task.WhenAll<Model.Authorization>(authzTasks);

                //TODO:DOCUMENT&TEST
                if (authz.All(au => au.Status == "valid") && order.Status == "pending")          //maybe we should set order status to ready?!
                {
                    using (tracer.BuildSpan("SetOrderReady").AsChildOf(tracer.ActiveSpan).StartActive(finishSpanOnDispose: true))
                    {
                        var orderUpdateDef = Builders<Model.Order>.Update.Set(o => o.Status, "ready");
                        await Orders.UpdateOneAsync(o => o.ID == request.Id, orderUpdateDef);
                    }
                    order = await Orders.AsQueryable().SingleAsync(o => o.ID == request.Id);
                }
                var retOrder = new Brick.Core.Order()
                {
                    Id = order.ID.ToString(),
                    Status = order.Status,
                    ExpiresDate = order.Expires,
                    CertificateId = order.CertId ?? "",
                    RequestedNotAfterDate = order.RequestedNotAfter,
                    RequestedNotBeforeDate = order.RequestedNotBefore,
                    Error = order.Error,
                    AccountId = order.AccountId.ToString(),
                };
                retOrder.Authz.AddRange(authz.Select(au =>
                {
                    var auth = new Brick.Core.Authorization
                    {
                        Id = au.Id.ToString(),
                        ExpiresDate = au.Expires,
                        Status = au.Status,
                        Identifier = new Brick.Core.Identifier
                        {
                            Type = au.Ident.Type,
                            Value = au.Ident.Value
                        },
                    };
                    auth.Challenges.AddRange(au.Challenges.Select(makeBrickChallengeFromModel).ToList());
                    return auth;
                }));
                return retOrder;
            }
        }
        public async override Task<Authorization> GetActiveAuthorization(AccountAndIdent request, ServerCallContext context)
        {
            var auth = await
                Authorizations.AsQueryable().SingleOrDefaultAsync(au => au.AccountId == request.AccountId && au.Ident.Type == request.Identifier.Type && au.Ident.Value == request.Identifier.Value && (au.Status == "pending" || au.Status == "valid"), cancellationToken: context.CancellationToken);
            if (auth == null)
                throw new RpcException(new Status(StatusCode.NotFound, "No suitable Auth found"));

            var returnAuth = new Brick.Core.Authorization()
            {
                Id = auth.Id,
                AccountId = auth.AccountId,
                ExpiresDate = auth.Expires,
                Identifier = request.Identifier,
                Status = auth.Status
            };
            returnAuth.Challenges.AddRange(auth.Challenges.Select(makeBrickChallengeFromModel));
            return returnAuth;
        }
        public async override Task<IdResponse> AddAuthorization(newAuthz request, ServerCallContext context)
        {
            var insertAuthz = new Model.Authorization()
            {
                Ident = new Model.Identifier()
                {
                    Type = request.Identifier.Type,
                    Value = request.Identifier.Value
                },
                Status = request.Challenges.Any(c => c.Type == "valid-01") ? "valid" : "pending",
                Expires = request.ExpiresDate,
                AccountId = request.AccountId
            };
            insertAuthz.Challenges = request.Challenges.Select(c => new Model.Challenge()
            {
                Type = c.Type,
                Status = c.Type == "valid-01" ? "valid" : "pending",
                Token = c.Token,
                ValidatedAt = null
            }).ToList();
            await Authorizations.InsertOneAsync(insertAuthz, cancellationToken: context.CancellationToken);
            return new IdResponse() { Id = insertAuthz.Id };
        }
        public async override Task GetAllAccounts(Empty request, IServerStreamWriter<Account> responseStream, ServerCallContext context)
        {
            await Accounts.AsQueryable().ForEachAsync(async (a) =>
            {
                var acc = new Account()
                {
                    CreatedAt = a.CreatedAt,
                    ExternalIdentifier = a.ExternalIdentifier ?? "",
                    Id = a.Id,
                    Status = a.Status
                };
                acc.Contact.AddRange(a.Contact);
                acc.Key = Google.Protobuf.ByteString.CopyFrom(a.Key);

                await responseStream.WriteAsync(acc);
            });
        }
        public async override Task GetAccountOrderIDs(IdRequest request, IServerStreamWriter<IdResponse> responseStream, ServerCallContext context)
        {
            var orderIds = Orders.AsQueryable().Where(o => o.AccountId == request.Id).Select(o => o.ID);
            var writeTasks = new List<Task>();
            foreach (var id in orderIds)
            {
                await responseStream.WriteAsync(new IdResponse() { Id = id });
            }
        }

        //Danger: Race Condition here!!
        public async override Task<Authorization> AddChallenge(addChallengeToAuthz request, ServerCallContext context)
        {
            var auth = await Authorizations.AsQueryable().SingleOrDefaultAsync(au => au.Id == request.Id);
            if (auth == null)
                throw new RpcException(new Status(StatusCode.NotFound, "Auth not found"));
            using (var scope = tracer.BuildSpan("mongo.AddChallenge(authId)").AsChildOf(tracer.ActiveSpan).WithTag("id", request.Id).StartActive(finishSpanOnDispose: true))
            {
                var authUpdateDef = Model.Authorization.CompleteUpdateDefinition(
                    Builders<Model.Authorization>.Update.AddToSet(au => au.Challenges, new Model.Challenge()
                    {
                        Status = request.Challenge.Status,
                        Token = request.Challenge.Token,
                        ValidatedAt = request.Challenge.Validated,
                        Type = request.Challenge.Type
                    }));
                var res = await Authorizations.UpdateOneAsync(au => au.Id == request.Id, authUpdateDef, cancellationToken: context.CancellationToken);
            }
            //TODO: Move this logic into brickweb
            if (request.Challenge.Status == "valid" && auth.Status != "valid")
            {
                using (var scope = tracer.BuildSpan("mongo.UpdateAuth(authId)").AsChildOf(tracer.ActiveSpan).WithTag("id", request.Id).StartActive(finishSpanOnDispose: true))
                {
                    var authUpdateDef = Model.Authorization.CompleteUpdateDefinition(Builders<Model.Authorization>.Update.Set(au => au.Status, "valid"));
                    await Authorizations.UpdateOneAsync(au => au.Id == request.Id, authUpdateDef);
                }
            }

            using (var scope = tracer.BuildSpan("mongo.GetAuth(Id)").AsChildOf(tracer.ActiveSpan).WithTag("id", request.Id).StartActive(finishSpanOnDispose: true))
                auth = await Authorizations.AsQueryable().SingleOrDefaultAsync(a => a.Id == request.Id, cancellationToken: context.CancellationToken);

            if (auth == null)
                throw new RpcException(new Status(StatusCode.NotFound, "Auth not found"));
            return makeBrickAuthFromModel(auth);
        }
        public async override Task<Authorization> UpdateAuthorization(updateAuthz request, ServerCallContext context)
        {
            Model.Authorization auth;
            using (var scope = tracer.BuildSpan("mongo.GetAuth(Id)").AsChildOf(tracer.ActiveSpan).WithTag("id", request.Id).StartActive(finishSpanOnDispose: true))
                auth = await Authorizations.AsQueryable().SingleOrDefaultAsync(a => a.Id == request.Id, cancellationToken: context.CancellationToken);
            var newChal = new Model.Challenge()
            {
                Status = request.UpdatedChallenge.Status,
                ID = request.UpdatedChallenge.Id,
                Token = request.UpdatedChallenge.Token,
                Type = request.UpdatedChallenge.Type,
                ValidatedAt = request.UpdatedChallenge.Validated
            };
            using (var scope = tracer.BuildSpan("UpdateAuthStatus").AsChildOf(tracer.ActiveSpan).StartActive(finishSpanOnDispose: true))
            {
                var updateDef = Model.Authorization.CompleteUpdateDefinition(Builders<Model.Authorization>.Update.Set(au => au.Status, request.NewStatus));
                var filterDef = Model.Authorization.CompleteFilterDefinition(auth.ConcurrencyControlToken,
                    Builders<Model.Authorization>.Filter.Eq(au => au.Id, auth.Id));
                var updateRes = await Authorizations.UpdateOneAsync(filterDef, updateDef);
                if (updateRes.ModifiedCount == 0)
                    throw new RpcException(new Status(StatusCode.DataLoss, "Could not update Auth Status due to concurrency conflict. Retry."));
                auth = await Authorizations.AsQueryable().SingleOrDefaultAsync(au => au.Id == request.Id);
            }
            using (var scope = tracer.BuildSpan("UpdateChallenge").AsChildOf(tracer.ActiveSpan).StartActive(finishSpanOnDispose: true))
            {
                logger.LogInformation("Updating {Challenge}", newChal);
                var authUpdateDef = Model.Authorization.CompleteUpdateDefinition(
                    Builders<Model.Authorization>.Update.Set(au => au.Challenges[-1], newChal));
                var filterDef = Model.Authorization.CompleteFilterDefinition(auth.ConcurrencyControlToken,
                    Builders<Model.Authorization>.Filter.ElemMatch(au => au.Challenges, chal => chal.ID == newChal.ID));
                var updateRes = await Authorizations.UpdateOneAsync(filterDef, authUpdateDef);
                if (updateRes.ModifiedCount == 0)
                    throw new RpcException(new Status(StatusCode.DataLoss, "Could not update Auth Status due to concurrency conflict. Retry."));
                auth = await Authorizations.AsQueryable().SingleAsync(au => au.Id == request.Id);
            }
            return makeBrickAuthFromModel(auth);
        }
        public async override Task<Authorization> GetAuthorization(IdRequest request, ServerCallContext context)
        {
            var authz = await Authorizations.AsQueryable().SingleOrDefaultAsync(a => a.Id == request.Id, cancellationToken: context.CancellationToken);
            if (authz == null)
                throw new RpcException(new Status(StatusCode.NotFound, "Auth not found"));
            return makeBrickAuthFromModel(authz);
        }
        public async override Task GetAuthorizationsForAccount(IdRequest request, IServerStreamWriter<Authorization> responseStream, ServerCallContext context)
        {
            var authz = await Authorizations.AsQueryable().Where(au => au.AccountId == request.Id).ToListAsync();
            foreach (var auth in authz)
            {
                await responseStream.WriteAsync(makeBrickAuthFromModel(auth));
            }
        }
        public async override Task<IdResponse> AddCertificate(NewCert request, ServerCallContext context)
        {
            using (var scope = tracer.BuildSpan("InputValidation").AsChildOf(tracer.ActiveSpan).StartActive(finishSpanOnDispose: true))
            {
                Ensure.That(await CaCertificates.AsQueryable().AnyAsync(ca => ca.Id == request.CaCertId)).IsTrue();
                Ensure.That(await Orders.AsQueryable().AnyAsync(o => o.ID == request.OrderId)).IsTrue();
            }
            var orderUpdateDef = Builders<Model.Order>.Update.Set(o => o.Status, "valid");
            Model.CaCertificate CaCert;
            using (var scope = tracer.BuildSpan("GetCaCert").AsChildOf(tracer.ActiveSpan).StartActive(finishSpanOnDispose: true))
            {
                CaCert = await CaCertificates.AsQueryable().SingleOrDefaultAsync(cacert => cacert.Id == request.CaCertId);
                if (CaCert == null)
                    throw new RpcException(new Status(StatusCode.NotFound, "The Associated CaCert does not exist"));
            }
            var newCert = new Model.Certificate()
            {
                CaCertificateId = request.CaCertId,
                OrderId = request.OrderId,
                IssuerNameHash = CaCert.NameHash,
                Raw = request.CertDER.ToArray(),
                Serial = request.Serial.ToArray()

            };
            using (var scope = tracer.BuildSpan("InsertCert").AsChildOf(tracer.ActiveSpan).StartActive(finishSpanOnDispose: true))
            {
                await Certificates.InsertOneAsync(newCert);
            }
            using (var scope = tracer.BuildSpan("UpdateOrder").AsChildOf(tracer.ActiveSpan).StartActive(finishSpanOnDispose: true))
                await Orders.UpdateOneAsync(o => o.ID == request.OrderId, orderUpdateDef);
            return new IdResponse() { Id = newCert.Id };
        }
        public async override Task<IdResponse> AddCaCertificate(CaCertificate request, ServerCallContext context)
        {
            var newCaCert = new Model.CaCertificate();
            newCaCert.Id = ObjectId.GenerateNewId().ToString();
            if (String.IsNullOrWhiteSpace(request.CaCertId))
            {
                request.CaCertId = newCaCert.Id;
            }
            else
            {
                using (var scope = tracer.BuildSpan("InputValidation").AsChildOf(tracer.ActiveSpan).StartActive(finishSpanOnDispose: true))
                {
                    try
                    {
                        Ensure.That(await CaCertificates.AsQueryable().AnyAsync(cacert => cacert.Id == request.CaCertId));
                    }
                    catch (ArgumentException e)
                    {
                        throw new RpcException(new Status(StatusCode.InvalidArgument, e.Message));
                    }
                }
            }
            var oldCaCertFilter = Builders<Model.CaCertificate>.Filter.And(
                Builders<Model.CaCertificate>.Filter.Eq(cacert => cacert.Raw, request.CertDER.ToArray()),
                Builders<Model.CaCertificate>.Filter.Eq(cacert => cacert.WillIssue, request.WillIssue));
            var oldCaCert = (await CaCertificates.FindAsync(oldCaCertFilter, cancellationToken: context.CancellationToken)).SingleOrDefault();
            if (oldCaCert != null)
            {
                tracer.ActiveSpan.Log(new Dictionary<string, object>()
                {
                    {"message", "CaCertificate already exists, returning existing" },
                    {"id", oldCaCert.Id }
                });
                return new IdResponse() { Id = oldCaCert.Id };
            };
            newCaCert.NameHash = request.NameHash.ToArray();
            newCaCert.Raw = request.CertDER.ToArray();
            newCaCert.WillIssue = request.WillIssue;
            newCaCert.CaCertId = request.CaCertId;
            newCaCert.CommonName = request.CommonName;
            await CaCertificates.InsertOneAsync(newCaCert, cancellationToken: context.CancellationToken);
            return new IdResponse() { Id = newCaCert.Id };
        }

        public async override Task<Empty> UpdateOrder(Order request, ServerCallContext context)
        {
            var oldOrder = await Orders.AsQueryable().SingleOrDefaultAsync(o => o.ID == request.Id);
            if (oldOrder == null)
            {
                throw new RpcException(new Status(StatusCode.NotFound, "Order not found"));
            }
            using (var scope = tracer.BuildSpan("InputValidation").AsChildOf(tracer.ActiveSpan).StartActive(finishSpanOnDispose: true))
            {
                Ensure.That(request.RequestedNotAfterDate).Is(oldOrder.RequestedNotAfter);
                Ensure.That(request.RequestedNotBeforeDate).Is(oldOrder.RequestedNotBefore);
                //TODO: Ensure no authz are trying to be added
            }
            var orderUpdateDef = Builders<Model.Order>.Update.Combine(
                Builders<Model.Order>.Update.Set(o => o.Status, request.Status),
                Builders<Model.Order>.Update.Set(o => o.CertId, String.IsNullOrWhiteSpace(request.CertificateId) ? null : request.CertificateId),
                Builders<Model.Order>.Update.Set(o => o.Error, request.Error));
            await Orders.UpdateOneAsync(o => o.ID == request.Id, orderUpdateDef);
            return new Empty();
        }

        public async override Task<Certificate> GetCertificate(IdRequest request, ServerCallContext context)
        {
            Model.Certificate cert;
            Model.CaCertificate cacert;
            using (var scope = tracer.BuildSpan("mongo.GetCert").AsChildOf(tracer.ActiveSpan).StartActive(finishSpanOnDispose: true))
                cert = await Certificates.AsQueryable().SingleOrDefaultAsync(c => c.Id == request.Id);
            if (cert == null)
                throw new RpcException(new Status(StatusCode.NotFound, "Cert not found"));
            using (var scope = tracer.BuildSpan("mongo.GetCaCert").AsChildOf(tracer.ActiveSpan).StartActive(finishSpanOnDispose: true))
                cacert = await CaCertificates.AsQueryable().SingleOrDefaultAsync(c => c.Id == cert.CaCertificateId);
            if (cacert == null)
                throw new RpcException(new Status(StatusCode.DataLoss, "CaCert not found"));
            var retCaCert = new Brick.Core.CaCertificate()
            {
                CaCertId = cacert.CaCertId,
                CertDER = Google.Protobuf.ByteString.CopyFrom(cacert.Raw),
                Id = cacert.Id.ToString(),
                NameHash = Google.Protobuf.ByteString.CopyFrom(cacert.NameHash),
                CommonName = cacert.CommonName,
                WillIssue = cacert.WillIssue
            };
            var ret = new Brick.Core.Certificate()
            {
                CaCert = retCaCert,
                CertDER = Google.Protobuf.ByteString.CopyFrom(cert.Raw),
                Id = cert.Id.ToString(),
                IssuerNameHash = Google.Protobuf.ByteString.CopyFrom(cert.IssuerNameHash),
                //TODO: Add Identifiers: Maybe to DB??
                OrderId = cert.OrderId,
                Serial = Google.Protobuf.ByteString.CopyFrom(cert.Serial),
                RevocationTime = cert.RevocationDate ?? ""
            };
            return ret;
        }

        public override async Task<Empty> RevokeCertificate(RevokeCert request, ServerCallContext context)
        {
            var cert = await Certificates.AsQueryable().SingleOrDefaultAsync(c => c.Id == request.Id);
            if (cert == null)
                throw new RpcException(new Status(StatusCode.NotFound, "Cert not found"));
            var setRevocationDate = Builders<Model.Certificate>.Update.Combine(
                Builders<Model.Certificate>.Update.Set(c => c.RevocationDate, DateTime.UtcNow.ToString("yyyy-MM-dd'T'HH:mm:ssZ")),
                Builders<Model.Certificate>.Update.Set(c => c.RevocationReason, request.RevocationReason));
            await Certificates.UpdateOneAsync(c => c.Id == request.Id, setRevocationDate);
            logger.LogInformation("Revoked a cert");
            return new Empty();
        }
        public async override Task<CaCertificate> GetCaCertificate(IdRequest request, ServerCallContext context)
        {
            var cacert = await CaCertificates.AsQueryable().SingleOrDefaultAsync(c => c.Id == request.Id);
            if (cacert == null)
                throw new RpcException(new Status(StatusCode.NotFound, "Cacert not found"));
            var ret = new Brick.Core.CaCertificate()
            {
                CaCertId = cacert.CaCertId,
                CertDER = Google.Protobuf.ByteString.CopyFrom(cacert.Raw),
                Id = cacert.Id.ToString(),
                NameHash = Google.Protobuf.ByteString.CopyFrom(cacert.NameHash),
                CommonName = cacert.CommonName,
                WillIssue = cacert.WillIssue
            };
            return ret;
        }
        public async override Task<Empty> StoreCRL(CRL request, ServerCallContext context)
        {
            return new Empty(); //HEHE
        }
        public override Task<CRL> GetCRL(IdRequest request, ServerCallContext context)
        {
            tracer.ActiveSpan.SetTag("caname", request.Id);
            throw new RpcException(new Status(StatusCode.NotFound, "Actually NYI, but sending NF"));
        }
        public async override Task<EnrichedChallenge> GetChallenge(IdRequest request, ServerCallContext context)
        {
            tracer.ActiveSpan.SetTag("id", request.Id);
            var auth = await Authorizations.AsQueryable().SingleOrDefaultAsync(a => a.Challenges.Any(c => c.ID == request.Id));
            if (auth == null)
            {
                throw new RpcException(new Status(StatusCode.NotFound, "Challenge does not exist"));
            }
            return new EnrichedChallenge
            {
                Challenge = auth.Challenges.Select(makeBrickChallengeFromModel).Single(c => c.Id == request.Id),
                AccountId = auth.AccountId,
                AuthorizationId = auth.Id
            };
        }
        public override async Task<RevokedCertificates> GetRevokedCertificates(IdRequest request, ServerCallContext context)
        {
            var revokedCerts = new RevokedCertificates();

            var a = await Certificates.AsQueryable().Where(cert => cert.RevocationDate != null).ToListAsync();
            revokedCerts.RevokedCerts.AddRange(a.Select(c => new RevokedCertificate()
            {
                RevocationTime = c.RevocationDate,
                Serial = Google.Protobuf.ByteString.CopyFrom(c.Serial)
            }));
            return revokedCerts;
        }
        public async override Task<Empty> UpdateAuthorizationStatus(newStatusForId request, ServerCallContext context)
        {
            try
            {
                var updateAuthDef = Model.Authorization.CompleteUpdateDefinition(Builders<Model.Authorization>.Update.Set(au => au.Status, request.Status));
                var x = await Authorizations.UpdateOneAsync(au => au.Id == request.Id, updateAuthDef);
                return new Empty();
            }
            catch (Exception e)
            {
                tracer.ActiveSpan.Log(new Dictionary<string, object>
                {
                    {"event", "error"},
                    {"error.object", e },
                });
                tracer.ActiveSpan.SetTag("error", true);
                throw new RpcException(new Status(StatusCode.Internal, "Could not update Authorization"));
            }
        }

        public async override Task<Empty> UpdateChallengeStatus(newStatusForId request, ServerCallContext context)
        {
            try
            {
                var updateDefinition = Model.Authorization.CompleteUpdateDefinition(Builders<Model.Authorization>.Update.Set(au => au.Challenges[-1].Status, request.Status));
                var filter = Builders<Model.Authorization>.Filter.ElemMatch(au => au.Challenges, chal => chal.ID == request.Id);
                var updateResult = await Authorizations.UpdateOneAsync(filter, updateDefinition);

                return new Empty();
            }
            catch (Exception e)
            {
                tracer.ActiveSpan.Log(new Dictionary<string, object>
                {
                    {"event", "error"},
                    {"error.object", e },
                });
                tracer.ActiveSpan.SetTag("error", true);
                throw new RpcException(new Status(StatusCode.Internal, "Could not update Challenge"));
            }
        }
        public async override Task<Certificate> GetCertificateBySerial(CertBySerial request, ServerCallContext context)
        {
            var filterDef = Builders<Model.Certificate>.Filter.And(
                Builders<Model.Certificate>.Filter.Eq(c => c.Serial, request.Serial.ToArray()),
                Builders<Model.Certificate>.Filter.Eq(c => c.IssuerNameHash, request.IssuerNameHash.ToArray()));
            var certs = await Certificates.FindAsync(filterDef, cancellationToken: context.CancellationToken);
            var cert = await certs.FirstOrDefaultAsync();
            if (cert == null)
                throw new RpcException(new Status(StatusCode.NotFound, "Certificate not found"));
            var cacert = await CaCertificates.AsQueryable().SingleAsync(c => c.Id == cert.CaCertificateId, context.CancellationToken);
            var retCaCert = new Brick.Core.CaCertificate()
            {
                CaCertId = cacert.CaCertId,
                CertDER = Google.Protobuf.ByteString.CopyFrom(cacert.Raw),
                Id = cacert.Id.ToString(),
                NameHash = Google.Protobuf.ByteString.CopyFrom(cacert.NameHash),
                CommonName = cacert.CommonName,
                WillIssue = cacert.WillIssue
            };
            var ret = new Brick.Core.Certificate()
            {
                CaCert = retCaCert,
                CertDER = Google.Protobuf.ByteString.CopyFrom(cert.Raw),
                Id = cert.Id.ToString(),
                IssuerNameHash = Google.Protobuf.ByteString.CopyFrom(cert.IssuerNameHash),
                //TODO: Add Identifiers: Maybe to DB??
                OrderId = cert.OrderId,
                Serial = Google.Protobuf.ByteString.CopyFrom(cert.Serial),
                RevocationTime = cert.RevocationDate ?? ""
            };
            return ret;
        }
        private Brick.Core.Challenge makeBrickChallengeFromModel(Model.Challenge c)
        {
            using (var scope = tracer.BuildSpan("makeBrickChallengeFromModel").AsChildOf(tracer.ActiveSpan).StartActive(finishSpanOnDispose: true))
            {
                return new Brick.Core.Challenge()
                {
                    Id = c.ID.ToString(),
                    Status = c.Status,
                    Validated = c.ValidatedAt ?? "",
                    Token = c.Token,
                    Type = c.Type
                };
            }
        }
        private Brick.Core.Authorization makeBrickAuthFromModel(Model.Authorization auth)
        {
            using (var scope = tracer.BuildSpan("makeBrickAuthFromModel").AsChildOf(tracer.ActiveSpan).StartActive(finishSpanOnDispose: true))
            {
                var returnAuth = new Brick.Core.Authorization()
                {
                    Id = auth.Id.ToString(),
                    AccountId = auth.AccountId.ToString(),
                    ExpiresDate = auth.Expires,
                    Identifier = new Brick.Core.Identifier()
                    {
                        Type = auth.Ident.Type,
                        Value = auth.Ident.Value
                    },
                    Status = auth.Status
                };
                returnAuth.Challenges.AddRange(auth.Challenges.Select(makeBrickChallengeFromModel));
                return returnAuth;
            }
        }
    }
}
