﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace brickstorage.Model
{
    public class Authorization
    {
        public static int i = 1;
        public static Func<Func<Authorization, bool>, int, Func<Authorization, bool>> ConcurrencyControl = (Func<Authorization, bool> predicate, int prevConcurrencyControlToken) => (Authorization au) => predicate(au) && au.ConcurrencyControlToken == prevConcurrencyControlToken;

        public Func<Authorization, bool> ConcurrencyControlThis(Func<Authorization, bool> predicate) => (Authorization au) => predicate(au) && au.ConcurrencyControlToken == this.ConcurrencyControlToken;
        public Authorization()
        {
            Challenges = new List<Challenge>();
            Id = ObjectId.GenerateNewId().ToString();
            ConcurrencyControlToken = 0;
        }
        [BsonId]
        public string Id { get; set; }
        public Identifier Ident;
        public string Status { get; set; }
        public string Expires { get; set; }
        public IList<Challenge> Challenges { get; set; }
        public string AccountId { get; set; }

        [BsonElement("version")]
        public int ConcurrencyControlToken { get; set; }
        public static async Task CreateIndices(IMongoCollection<Authorization> coll, CancellationToken token = default(CancellationToken))
        {
            var certIndex = Builders<Authorization>.IndexKeys.Ascending(au => au.AccountId);
            await coll.Indexes.CreateOneAsync(new CreateIndexModel<Authorization>(certIndex), cancellationToken: token);
            certIndex = Builders<Authorization>.IndexKeys.Combine(
                certIndex = Builders<Authorization>.IndexKeys.Ascending(au => au.AccountId),
                certIndex = Builders<Authorization>.IndexKeys.Ascending(au => au.Ident.Value)
            );
            await coll.Indexes.CreateOneAsync(new CreateIndexModel<Authorization>(certIndex), cancellationToken: token);
        }

        /// <summary>
        /// Completes the passed list of UpdateDefinitions by combining them, and adding the Inc(ConcurrencyControlToken)
        /// </summary>
        /// <param name="defs"></param>
        /// <returns></returns>
        public static UpdateDefinition<Model.Authorization> CompleteUpdateDefinition(params UpdateDefinition<Model.Authorization>[] defs)
        {
            var udefs = new List<UpdateDefinition<Model.Authorization>>(defs);
            udefs.Add(Builders<Model.Authorization>.Update.Inc(au => au.ConcurrencyControlToken, 1));
            return Builders<Model.Authorization>.Update.Combine(udefs);
        }
        public static FilterDefinition<Authorization> CompleteFilterDefinition(int prevConcurrencyControlToken, params FilterDefinition<Model.Authorization>[] defs)
        {
            var fdefs = new List<FilterDefinition<Authorization>>(defs);
            fdefs.Add(Builders<Model.Authorization>.Filter.Eq(au => au.ConcurrencyControlToken, prevConcurrencyControlToken));
            return Builders<Authorization>.Filter.And(fdefs);
        }
    }
}
