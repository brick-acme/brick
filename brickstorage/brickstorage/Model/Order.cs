﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace brickstorage.Model
{
    class Order
    {
        public Order()
        {
            ID = ObjectId.GenerateNewId().ToString();
        }
        [BsonId]
        public string ID { get; set; }
        public string Status { get; set; }
        public string Expires { get; set; }
        public string RequestedNotBefore { get; set; }
        public string RequestedNotAfter { get; set; }
        public Brick.Core.Problem Error { get; set; } = null;
        public IEnumerable<string> AuthzIDs { get; set; }
        public string CertId { get; set; }
        public string AccountId { get; set; }
    }
}
