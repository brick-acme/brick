﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace brickstorage.Model
{
    public class Challenge
    {
        public Challenge()
        {
            ID = ObjectId.GenerateNewId().ToString();
        }
        [BsonId]
        public string ID { get; set; }
        public string Type { get; set; }
        public string Status { get; set; }
        public string Token { get; set; }  //May or may not be set (e.g. OOB-Authz)
        public string ValidatedAt { get; set; }
    }
}
