﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Text;

namespace brickstorage.Model
{
    class Account
    {
        //An Account gets its ID from Brickweb - no generation of IDs here on in the Database
        public Account()
        {
            Contact = new List<string>();
        }
        [BsonId]
        public ObjectId _id { get; set; }
        public string Id { get; set; }
        public byte[] Key { get; set; }
        public string Status { get; set; }
        public string CreatedAt { get; set; }
        public List<string> Contact { get; set; }
        public string ExternalIdentifier { get; set; }

        public static void CreateIndices(IMongoCollection<Account> collection)
        {

        }
    }

}
