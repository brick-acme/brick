﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace brickstorage.Model
{
    class CRL
    {
        public CRL()
        {
            ID = ObjectId.GenerateNewId().ToString();
        }
        [BsonId]
        public string ID { get; set; }

        public string ValidFrom { get; set; }
        public string ValidTo { get; set; }
        public BsonBinaryData Crl_Binary { get; set; }
    }
}
