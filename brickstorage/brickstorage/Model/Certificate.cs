﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace brickstorage.Model
{
    class Certificate
    {
        public Certificate()
        {
            Id = ObjectId.GenerateNewId().ToString();
        }
        [BsonId]
        public string Id { get; set; }
        public byte[] Raw { get; set; }
        public string OrderId { get; set; }
        public string CaCertificateId { get; set; }
        public byte[] IssuerNameHash { get; set; }
        public string RevocationDate { get; set; }
        public string RevocationReason { get; set; }
        public byte[] Serial { get; set; }


        public static async Task CreateIndices(IMongoCollection<Certificate> coll, CancellationToken token = default(CancellationToken))
        {
            var certIndex = Builders<Model.Certificate>.IndexKeys.Ascending(c => c.CaCertificateId);
            await coll.Indexes.CreateOneAsync(new CreateIndexModel<Certificate>(certIndex), cancellationToken: token);
            certIndex = Builders<Model.Certificate>.IndexKeys.Ascending(c => c.IssuerNameHash);
            await coll.Indexes.CreateOneAsync(new CreateIndexModel<Certificate>(certIndex), cancellationToken: token);
            certIndex = Builders<Model.Certificate>.IndexKeys.Ascending(c => c.OrderId);
            await coll.Indexes.CreateOneAsync(new CreateIndexModel<Certificate>(certIndex), cancellationToken: token);
        }
    }
}
