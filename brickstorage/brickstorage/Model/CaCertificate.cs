﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace brickstorage.Model
{
    class CaCertificate
    {
        public CaCertificate()
        {
            Id = ObjectId.GenerateNewId().ToString();
        }
        [BsonId]
        public string Id { get; set; }
        public byte[] Raw { get; set; }
        public byte[] NameHash { get; set; }
        public bool WillIssue { get; set; }
        public string CaCertId { get; set; }
        public string CommonName { get; set; }
    }
}
