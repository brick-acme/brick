﻿using System;
using System.Collections.Generic;
using System.Text;

namespace brickstorage.Model
{
    public class Identifier
    {
        public string Type { get; set; }
        public string Value { get; set; }
    }
}
