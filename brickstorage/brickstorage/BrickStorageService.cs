﻿using Grpc.Core;
using Grpc.Core.Interceptors;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using MongoDB.Driver;
using OpenTracing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace brickstorage
{
    class BrickStorageService : IHostedService
    {
        private IMongoDatabase db;
        private ILogger logger;
        private ITracer tracer;
        private Server server;
        private IEnumerable<Interceptor> interceptors;
        public BrickStorageService(IMongoDatabase brickdb, ILogger<BrickStorageService> l, ITracer t, IEnumerable<Interceptor> ints)
        {
            logger = l;
            tracer = t;
            db = brickdb;
            interceptors = ints;
        }
        public Task StartAsync(CancellationToken cancellationToken)
        {
            server = new Server
            {
                Services =
                {
                    Brick.StorageAuthority.StorageAuthority.BindService(new BrickStorageAuthorityImpl(db, logger, tracer)).Intercept(interceptors.ToArray()),
                    Grpc.Health.V1.Health.BindService(new HealthCheckImpl(db, logger))
                },
                Ports = { new ServerPort("0.0.0.0", 9000, ServerCredentials.Insecure) }
            };
            server.Start();
            logger.LogInformation("Started Serving");
            return Task.CompletedTask;
        }
        public async Task StopAsync(CancellationToken cancellationToken)
        {
            await server.ShutdownAsync();
        }
    }
}
