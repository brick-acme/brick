﻿using Microsoft.Extensions.Logging;
using OpenTracing;
using System;

namespace brickstorage
{
    class TracerBuilder
    {
        private ILoggerFactory _fac;
        private ITracer _tracer;
        private ILogger Logger;
        public TracerBuilder(ILoggerFactory fac)
        {
            _fac = fac;
            Logger = _fac.CreateLogger("TracerBuilder");
        }
        public ITracer Tracer
        {
            get
            {
                if (_tracer is null)
                {
                    Environment.SetEnvironmentVariable("JAEGER_SERVICE_NAME", "brickstorage");
                    if (Environment.GetEnvironmentVariable("JAEGER_SAMPLER_TYPE") == null)
                    {
                        Logger.LogWarning("Set JAEGER_SAMPLER_TYPE to const, and JAEGER_SAMPLER_PARAM to 1.0, because it is unset");
                        Environment.SetEnvironmentVariable("JAEGER_SAMPLER_TYPE", "const");
                        Environment.SetEnvironmentVariable("JAEGER_SAMPLER_PARAM", "1.0");
                    }
                    var conf = Jaeger.Configuration.FromEnv(_fac);
                    Logger.LogInformation("jaegerconf: {0}", conf.ReporterConfig.SenderConfig);
                    _tracer = conf.GetTracer();
                    OpenTracing.Util.GlobalTracer.Register(_tracer);
                }
                return _tracer;
            }
            private set
            {
                _tracer = value;
            }
        }

    }
}
