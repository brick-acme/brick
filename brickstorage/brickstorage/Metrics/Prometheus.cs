﻿using Prometheus;
using static Prometheus.Metrics;
using System;
using System.Collections.Generic;
using System.Text;

namespace brickstorage.Metrics
{
    static public class Custom
    {
        public static Counter MongoCalls = CreateCounter("mongo_calls_total", "Total MongoDB Calls", new CounterConfiguration()
        {
            LabelNames = new[] { "verb" }
        });
    }
}
