﻿using OpenTracing.Contrib.Grpc.Interceptors;
using Grpc.Core.Interceptors;
using MongoDB.Driver;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.DependencyInjection;
using OpenTracing;
using Microsoft.Extensions.Configuration;
using MongoDB.Driver.Core.Events;
using OpenTracing.Extensions;
using MongoDB.Bson;
using Serilog;
using Prometheus;

namespace brickstorage
{
    public static class Program
    {
        private static bool consul = true;
        public static async Task Main(string[] args)
        {
            var metricServer = new KestrelMetricServer(port: 8999);
            metricServer.Start();
            Log.Logger = new LoggerConfiguration()
                .Enrich.FromLogContext()
                .WriteTo.Console()
                .CreateLogger();
            var builder = new HostBuilder()
                .ConfigureAppConfiguration((hostingContext, configBuilder) =>
                {
                    configBuilder.AddJsonFile("appsettings.json");
                    configBuilder.AddEnvironmentVariables();
                })
                .ConfigureServices((hostContext, services) =>
                {
                    services.AddOptions();
                    services.AddSingleton<Interceptor, ServerTracingInterceptor>();
                    services.AddSingleton<TracerBuilder>();
                    services.AddSingleton<ITracer>((svcs) => svcs.GetRequiredService<TracerBuilder>().Tracer);
                    services.AddSingleton<IMongoClient>(provider =>
                    {
                        var serverSettings = MongoClientSettings.FromConnectionString(provider.GetRequiredService<IConfiguration>()["mongo:url"]);
                        serverSettings.ClusterConfigurator = cp =>
                        {
                            cp.Subscribe<CommandStartedEvent>(e =>
                            {
                                Metrics.Custom.MongoCalls.WithLabels(e.CommandName).Inc();
                                var span = provider.GetRequiredService<ITracer>().ActiveSpan;
                                if (span != null)
                                {
                                    span.LogDBStatement("brick", $"{e.CommandName}({e.Command.ToJson()})", "mongo");
                                }
                            });
                        };
                        return new MongoClient(serverSettings);
                    });
                    services.AddOpenTracingCoreServices(otBuilder =>
                    {
                        otBuilder.AddCoreFx();
                        otBuilder.AddLoggerProvider();
                    });

                    services.AddSingleton<IMongoDatabase>(provider => provider.GetRequiredService<IMongoClient>().GetDatabase("brick"));
                    services.AddHostedService<BrickStorageService>();
                })
                .ConfigureLogging((hostContext, logging) =>
                {
                    logging.AddSerilog(dispose: true);
                });

            //This is fire-and-forget async because we don't care if it fails - it's mainly for development
            //If consul isn't running, then we don't want to register anyway
            if (consul)
#pragma warning disable CS4014 // Because this call is not awaited, execution of the current method continues before the call is completed
                Task.Run(() => brickstorage.Integrations.Consul.RegisterWithConsul());
#pragma warning restore CS4014 // Because this call is not awaited, execution of the current method continues before the call is completed

            await builder
                .RunConsoleAsync();
            return;
        }
    }
}
