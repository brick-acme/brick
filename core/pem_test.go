/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

package core_test

import (
	"brick/core"
	"errors"
	"io/ioutil"
	"testing"
)

func TestPEM(t *testing.T) {
	c := &core.Certificate{}
	var err error
	c.Cert, err = core.LoadPEMCertificate("./testdata/root.pem")
	if err != nil {
		t.Error(err)
	}
	c.DER = c.Cert.Raw
	bytes := c.PEM()
	file, err := ioutil.ReadFile("./testdata/root.pem")
	if err != nil {
		t.Error(err)
	}
	for i, _ := range bytes {
		if file[i] != bytes[i] {
			t.Errorf("File differs from parsed PEM on position %d\n", i)
			err = errors.New("File differs")
		}
	}
	if err != nil {
		t.Log("Original PEM:\n")
		b, _ := ioutil.ReadFile("./testdata/root.pem")
		t.Log(string(b))
		t.Log("\nCreated PEM:\n")
		t.Log(string(bytes))
	}

}
