﻿using ACMESharp.Protocol.Resources;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace brickintegration.Tests
{
    class AddAccountandLookup : ITest
    {
        private readonly IList<Exception> _exceptions = new List<Exception>();

        public async Task<TestResult> RunAsync(CancellationToken token)
        {
            try
            {
                var client = new ACMESharp.Protocol.AcmeProtocolClient(new Uri(brickintegration.Global.Statics.Uri), new ServiceDirectory() { Directory = "/dir" });
                var acmeDir = await client.GetDirectoryAsync();
                client.Directory = acmeDir;
                await client.GetNonceAsync();
                token.ThrowIfCancellationRequested();
                Console.WriteLine(client.NextNonce);
                var accDetails = await client.CreateAccountAsync(new[] { "random@email.com" });
                token.ThrowIfCancellationRequested();
                var accDetails2 = await client.CheckAccountAsync();
                token.ThrowIfCancellationRequested();
                client.Account = accDetails;
                if (accDetails.Kid != accDetails.Kid)
                    throw new Exception("Kid of new and old accounts do not match");
                var accDetails3 = await client.UpdateAccountAsync(new[] { "moritz.basel@datev.de" });
                token.ThrowIfCancellationRequested();
                return TestResult.OK(Name);
            }
            catch (Exception e)
            {
                return TestResult.Bad(Name, e);
            }
        }

        public IEnumerable<Exception> Errors
        {
            get
            {
                return _exceptions;
            }
        }

        public string Name => "AddAccountAndLookup";
    }
}
