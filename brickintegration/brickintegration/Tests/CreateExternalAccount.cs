﻿using ACMESharp.Protocol;
using ACMESharp.Protocol.Resources;
using brickintegration.TestUtils;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace brickintegration.Tests
{
    class CreateExternalAccount : ITest
    {
        public string Name => "CreateExternalAccount";

        public AcmeProtocolClient client { get; set; }

        public async Task<TestResult> RunAsync(CancellationToken token)
        {
            try
            {
                client = new ACMESharp.Protocol.AcmeProtocolClient(new Uri(brickintegration.Global.Statics.Uri), new ServiceDirectory() { Directory = "/dir" });
                client.Directory = await client.GetDirectoryAsync();
                await client.GetNonceAsync();
                client.Account = await client.CreateAccountAsync(new[] { "random@email.com" }, externalAccountBinding: new { token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYW1lIjoiVDI5NDA5QSIsImV4cCI6MTU0MjM4OTE1MSwiaXNzIjoiZGF0ZXYuZGUiLCJhdWQiOiJkYXRldi5kZSJ9.nGDUcFDakr0BZUhDLR6kuyzJmZVPMy2AaEkoGb-yaek" });
                return TestResult.OK(Name);
            }
            catch (Exception e)
            {
                return TestResult.Bad(Name, e);
            }

        }
    }
}
