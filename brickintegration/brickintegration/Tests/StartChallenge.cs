﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using brickintegration.TestUtils;
using System.Linq;
using EnsureThat;

namespace brickintegration.Tests
{
    class StartChallenge : ITest, WithClient, WithOrder
    {
        public ACMESharp.Protocol.AcmeProtocolClient client { get; set; }
        public ACMESharp.Protocol.OrderDetails Order { get; set; }

        public string Name => "StartChallenge";

        public async Task<TestResult> RunAsync(CancellationToken t)
        {
            try
            {
                await this.CreateClient();
                await this.CreateOrder(new[] { "localhost" }, t);
                var authz = await client.GetAuthorizationDetailsAsync(Order.Payload.Authorizations[0]);
                var chal = await client.AnswerChallengeAsync(authz.Challenges.Where(c => c.Type == "http-01").First().Url);
                Ensure.That(chal.Status).Is("processing");
                return TestResult.OK(Name);
            }
            catch (Exception e)
            {
                return TestResult.Bad(Name, e);
            }
        }
    }
}
