﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using EnsureThat;
using Org.BouncyCastle.X509;
using System.IO;
using brickintegration.TestUtils;
using System.Threading;

namespace brickintegration.Tests
{
    class AddTwoOrders :  ITest, WithClient
    {
        public ACMESharp.Protocol.AcmeProtocolClient client { get; set; }

        public string Name => "AddTwoOrders";

        public async Task<TestResult> RunAsync(CancellationToken t)
        {
            await this.CreateClient();
            t.ThrowIfCancellationRequested();
            try
            {
                var order1 = await client.CreateOrderAsync(new[] { "localhost" }, notBefore: DateTime.UtcNow, notAfter: DateTime.UtcNow.Add(TimeSpan.FromDays(400)));
                t.ThrowIfCancellationRequested();
                var order2 = await client.CreateOrderAsync(new[] { "localhost", "blocalhost" });
                t.ThrowIfCancellationRequested();
                Ensure.That(order2.Payload.Authorizations.Contains(order1.Payload.Authorizations[0])).IsTrue();
            }
            catch (Exception e)
            {
                return TestResult.Bad(Name, e);
            }
            return TestResult.OK(Name);
        }
    }
}
