﻿using brickintegration.TestUtils;
using EnsureThat;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Linq;

namespace brickintegration.Tests
{
    class AddOrderAndGetAuthzs : ITest, WithClient
    {
        public ACMESharp.Protocol.AcmeProtocolClient client { get; set; }

        public string Name => "AddOrderAndGetAuthz";

        public async Task<TestResult> RunAsync(CancellationToken token)
        {
            try
            {
                await this.CreateClient();
                token.ThrowIfCancellationRequested();
                var order = await client.CreateOrderAsync(new[] { "localhost" });
                token.ThrowIfCancellationRequested();
                if (order.Payload.Authorizations.Length != 1)
                {
                    throw new Exception($"Expected 1 Authorization but got {order.Payload.Authorizations.Length}");
                }
                var auth = await client.GetAuthorizationDetailsAsync(order.Payload.Authorizations[0]);
                Ensure.That(auth.Status).Is("pending");
                Ensure.That(auth.Challenges.Any(c => c.Type == "http-01")).IsTrue();
                Ensure.That(!auth.Challenges.Any(c => c.Status != "pending"));
            }
            catch (Exception e)
            {
                return TestResult.Bad(Name, e);
            }
            return TestResult.OK(Name);
        }
    }
}
