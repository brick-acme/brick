﻿using EnsureThat;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Linq;
using brickintegration.TestUtils;
using System.Threading;

namespace brickintegration.Tests
{
    class AddAndRetrieveOrder : ITest, WithClient
    {
        public ACMESharp.Protocol.AcmeProtocolClient client { get; set; }

        public string Name => "AddAndRetrieveOrder";

        public async Task<TestResult> RunAsync(CancellationToken t)
        {
            await this.CreateClient();
            t.ThrowIfCancellationRequested();
            try
            {
                var order = await client.CreateOrderAsync(new[] { "localhost", "blocalhost" });
                t.ThrowIfCancellationRequested();
                var order2 = await client.GetOrderDetailsAsync(order.OrderUrl);
                t.ThrowIfCancellationRequested();
                var firstMap = order.Payload.Authorizations.GroupBy(x => x).ToDictionary(x => x.Key, x => x.Count());
                var secondMap = order2.Payload.Authorizations.GroupBy(x => x).ToDictionary(x => x.Key, x => x.Count());
                Ensure.That(firstMap.Keys.All(x => secondMap.Keys.Contains(x) && firstMap[x] == secondMap[x])).IsTrue();
  
                return TestResult.OK(Name);
            }
            catch (Exception e)
            {
                return TestResult.Bad(Name, e);
            }
        }
    }
}
