﻿using brickintegration.TestUtils;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace brickintegration.Tests
{
    class DeactivateAccount : ITest, WithClient
    {
        public ACMESharp.Protocol.AcmeProtocolClient client { get; set; }

        public string Name => "DeactivateAccount";

        public async Task<TestResult> RunAsync(CancellationToken t)
        {
            await this.CreateClient();
            t.ThrowIfCancellationRequested();
            try
            {
                await client.DeactivateAccountAsync();
                t.ThrowIfCancellationRequested();
                var acc = await client.UpdateAccountAsync(new[] { "myself" });
                t.ThrowIfCancellationRequested();
                if (acc.Payload.Status != "deactivated")
                {
                    throw new Exception($"Account status should be deactivated after deactivating. Was {acc.Payload.Status}");
                }
            }
            catch (Exception e)
            {
                return TestResult.Bad(Name, e);
            }
            return TestResult.OK(Name);
        }
    }
}
