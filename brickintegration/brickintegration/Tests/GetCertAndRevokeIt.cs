﻿using ACMESharp.Protocol;
using EnsureThat;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using brickintegration.TestUtils;
using System.Threading;

namespace brickintegration.Tests
{
    class GetCertAndRevokeIt : ITest, WithClient
    {
        public AcmeProtocolClient client { get; set; }

        public string Name => "GetCertAndRevokeIt";

        public async Task<TestResult> RunAsync(CancellationToken t)
        {
            await this.CreateClient();
            t.ThrowIfCancellationRequested();
            try
            { 
                var order1 = await client.CreateOrderAsync(new[] { "localhost.local" });
                var url = order1.OrderUrl;
                Ensure.That(url).IsNotNullOrWhiteSpace();
                order1 = await client.GetOrderDetailsAsync(order1.OrderUrl);
                t.ThrowIfCancellationRequested();
                var bytes = File.ReadAllBytes("csr.der");
                order1 = await client.FinalizeOrderAsync(order1.Payload.Finalize, bytes);
                t.ThrowIfCancellationRequested();
                Ensure.That(order1.Payload.Status).Is("processing");
                await Task.Delay(2000);
                order1 = await client.GetOrderDetailsAsync(url);
                t.ThrowIfCancellationRequested();
                Ensure.That(order1.Payload.Status).Is("valid");
                var cert = await client.GetOrderCertificateAsync(order1);
                t.ThrowIfCancellationRequested();
                Console.WriteLine(Encoding.ASCII.GetString(cert));
                //TODO REVOKED
            }
            catch (Exception e)
            {
                return TestResult.Bad(Name, e);
            }
            return TestResult.OK(Name);
        }
    }
}
