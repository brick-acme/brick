﻿using ACMESharp.Protocol;
using brickintegration.TestUtils;
using EnsureThat;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace brickintegration.Tests
{
    class AddAndFinalizeDefinitelyReadyOrder : ITest, WithClient
    {

        public string Name => "AddAndFinalizeDefinitelyReadyOrder";

        public AcmeProtocolClient client { get; set; }

        public async Task<TestResult> RunAsync(CancellationToken t)
        {
            await this.CreateClient();
            t.ThrowIfCancellationRequested();
            try
            {
                var order1 = await this.client.CreateOrderAsync(new[] { "localhost.local" });
                t.ThrowIfCancellationRequested();
                var url = order1.OrderUrl;
                Ensure.That(url).IsNotNullOrWhiteSpace();
                order1 = await client.GetOrderDetailsAsync(order1.OrderUrl);
                t.ThrowIfCancellationRequested();
                var bytes = File.ReadAllBytes("csr.der");
                order1 = await client.FinalizeOrderAsync(order1.Payload.Finalize, bytes);
                t.ThrowIfCancellationRequested();
                Ensure.That(order1.Payload.Status).Is("processing");
                await Task.Delay(2000);
                order1 = await client.GetOrderDetailsAsync(url);
                t.ThrowIfCancellationRequested();
                Ensure.That(order1.Payload.Status).Is("valid");
                var cert = await client.GetOrderCertificateAsync(order1);
                t.ThrowIfCancellationRequested();
                //TODO: Add Verification if the certificate is well-formed
                
            }
            catch (Exception e)
            {
                return TestResult.Bad(Name, e);
            }
            return TestResult.OK(Name);
        }
    }
}
