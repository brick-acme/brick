﻿using ACMESharp.Protocol.Resources;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace brickintegration.TestUtils
{
    public interface WithClient
    {
        ACMESharp.Protocol.AcmeProtocolClient client { get; set; }
    }

    public static class WithClientExtension
    {
        public async static Task CreateClient(this WithClient test)
        {
            var client = new ACMESharp.Protocol.AcmeProtocolClient(new Uri(brickintegration.Global.Statics.Uri), new ServiceDirectory() { Directory = "/dir" });
            client.Directory = await client.GetDirectoryAsync();
            await client.GetNonceAsync();
            client.Account = await client.CreateAccountAsync(new[] { "random@email.com" });
            test.client = client;
        }
    }
}
