﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace brickintegration.TestUtils
{
    public interface WithOrder : WithClient
    {
        ACMESharp.Protocol.OrderDetails Order { get; set; }
    }

    public static class WithOrderExtensions
    {
        public static async Task CreateOrder(this WithOrder test, string[] identifiers, CancellationToken t = default)
        {
            t.ThrowIfCancellationRequested();
            test.Order = await test.client.CreateOrderAsync(identifiers);
            t.ThrowIfCancellationRequested();
            return;
        }
    }
}
