﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace brickintegration
{
    class TestCollection
    {
        private IList<ITest> _tests = new List<ITest>();
        public void AddTest(ITest t)
        {
            _tests.Add(t);
        }

        public async Task<int> RunAsync()
        {
            var cancellationTokenSource = new CancellationTokenSource(10000);
            var tasks = _tests.Select(async t =>
            {
                return await t.RunAsync(cancellationTokenSource.Token);
            });
            var results = await Task.WhenAll(tasks);
            Console.WriteLine($"{results.Length} Tests Finished");
            Console.WriteLine($"{results.Count(r => r.Success)} Tests successful");
            if (results.Any(r => r.Success == false))
                Console.Error.WriteLine("Tests failed");
            results.Where(r => r.Success == false).ToList().ForEach(x =>
            {
                Console.Error.WriteLine($"Test: {x.Test} unsuccessful");
                Console.Error.WriteLine(x.Error);
            });
            return -results.Count(r => r.Success == false);
        }
    }
}
