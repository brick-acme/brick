﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace brickintegration
{
    interface ITest
    {
        Task<TestResult> RunAsync(CancellationToken token);
        string Name { get; }
    }

    public class TestResult
    {
        public bool Success;
        public Exception Error;
        public string Detail;
        public string Test;

        public static TestResult OK(string callingTest)
        {
            return new TestResult()
            {
                Test = callingTest,
                Success = true
            };
        }
        public static TestResult Bad(string callingTest, Exception e, string Detail = null)
        {
            return new TestResult()
            {
                Test = callingTest,
                Success = false,
                Error = e,
                Detail = Detail
            };
        }
    }


}
