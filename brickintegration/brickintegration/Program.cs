﻿using System;
using System.Diagnostics;
using System.Net.Http;
using System.Threading.Tasks;

namespace brickintegration
{
    class Program
    {
        static int Main(string[] args)
        {
            if (args != null && args.Length > 0)
            {
                brickintegration.Global.Statics.Uri = args[0];
            }
            else
            {
                brickintegration.Global.Statics.Uri = "http://localhost";
            }
            Console.WriteLine($"Using {brickintegration.Global.Statics.Uri}");
            while (true)
                RunTests();
        }
        static int RunTests()
        {
            TestCollection tests = new TestCollection();
            tests.AddTest(new brickintegration.Tests.AddAccountandLookup());
            tests.AddTest(new Tests.AddAndRetrieveOrder());
            tests.AddTest(new Tests.DeactivateAccount());
            tests.AddTest(new Tests.AddTwoOrders());
            tests.AddTest(new Tests.AddAndFinalizeDefinitelyReadyOrder());
            tests.AddTest(new Tests.StartChallenge());
            tests.AddTest(new Tests.GetCertAndRevokeIt());
            //tests.AddTest(new Tests.CreateExternalAccount());
            int res = tests.RunAsync().Result;
            if (res < 0)
            {
                System.Threading.Thread.Sleep(6000);
            }
            return res;
        }
    }
}
