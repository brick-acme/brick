/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

package main

import (
	"fmt"
	"net/http"

	"github.com/prometheus/client_golang/prometheus/promhttp"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

func (srv *caServer) handleError(code codes.Code, format string, args ...interface{}) (interface{}, error) {
	return nil, status.Errorf(code, format, args...)
}

func StartPrometheus(port int) {
	hndlr := promhttp.Handler()
	http.Handle("/metrics", hndlr)
	panic(http.ListenAndServe(fmt.Sprintf(":%d", port), nil))
}
