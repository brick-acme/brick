/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
//Package metrics contains all the metrics we collect in brickca
package metrics

import (
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
)

var (
	//CertsCreated counts the Certificates signed
	CertsCreated = promauto.NewCounter(prometheus.CounterOpts{
		Name: "brickca_certs_created_total",
		Help: "Total Number of Certificates signed/created",
	})
	//CrlsSigned counts the CRLs signed
	CrlsSigned = promauto.NewCounter(prometheus.CounterOpts{
		Name: "brickca_crls_signed_total",
		Help: "CRLs signed by BrickCA",
	})
)
