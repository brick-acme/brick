/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

package policy

import (
	"context"
	"crypto/x509"

	"brick/brickca/common"
)

//CaPolicy is the
type CaPolicy struct {
	checkers []Checker
}

func New() *CaPolicy {
	c := make([]Checker, 0)
	return &CaPolicy{c}
}

//CheckPolicy checks the passed Certification Request through all internal Checkers
func (p *CaPolicy) CheckPolicy(ctx context.Context, req *x509.CertificateRequest, cacert common.Cacert) error {
	var err error
	for _, check := range p.checkers {
		if err == nil {
			err = check(ctx, req, cacert)
		}
	}
	return err
}

//AddChecker adds a Policy Checker
func (p *CaPolicy) AddChecker(check Checker) {
	p.checkers = append(p.checkers, check)
}

//Checker is a pure function for checking a certain Certification Signing Request against arbitrary CA-Policies
// - say extensions, identifier violations etc.
type Checker func(context.Context, *x509.CertificateRequest, common.Cacert) error
