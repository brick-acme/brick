/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

package common

import (
	"context"
	"crypto/x509"
	"time"
)

//Cacert represents a selection of a CaCert the PKCS#10 Certificate Request pertains to. May be empty string for "any" CA
type Cacert string

//CertCompleted is the data that is passed from the caProvider back to the storage-aware frontend
type CertCompleted struct {
	Cert      *x509.Certificate
	CaCert    *x509.Certificate
	OrderID   string
	Err       error
	Ctx       context.Context
	StartTime time.Time
}
