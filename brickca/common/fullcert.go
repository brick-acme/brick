/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

package common

import (
	proto "brick/grpc/corepb"
	"crypto/x509"
)

//FullCert represents a complete Certificate as it has been issued and stored
type FullCert struct {
	Proto *proto.Certificate
	X509  *x509.Certificate
}

//FullCaCert holds a Proto and x509 Certificate
type FullCaCert struct {
	Proto *proto.CaCertificate
	X509  *x509.Certificate
}
