/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

package main

import (
	"context"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	"github.com/opentracing/opentracing-go"

	h "google.golang.org/grpc/health/grpc_health_v1"
)

type healthServer struct {
	checkFunc func(context.Context) bool
}

func (health *healthServer) Check(rootCtx context.Context, req *h.HealthCheckRequest) (*h.HealthCheckResponse, error) {
	span, ctx := opentracing.StartSpanFromContext(rootCtx, "Check")
	defer span.Finish()
	if req.Service == "" || req.Service == "brickca" {
		if health.checkFunc(ctx) {
			return &h.HealthCheckResponse{
				Status: h.HealthCheckResponse_SERVING,
			}, nil
		}
	}
	return &h.HealthCheckResponse{
		Status: h.HealthCheckResponse_NOT_SERVING,
	}, nil
}

func (health *healthServer) Watch(req *h.HealthCheckRequest, w h.Health_WatchServer) error {
	return status.Error(codes.Unimplemented, "Watch is Unimplemented")
}
