/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package ca

import (
	"encoding/binary"
	"math/big"
	"math/rand"
	"sync"
	"sync/atomic"
)

//serialGen is unused for now, but intended to implement the SerialProvider interface and possibly made public
type serialGen struct {
	SerialPrefix byte
	serial       *big.Int
	serialMut    sync.Mutex
}

//SerialProvider represents the system a CA uses to generate Serial numbers for their Certificates
type SerialProvider interface {
	GetNewSerial() *big.Int
}

type SerialProviderFunc func() *big.Int

func (s SerialProviderFunc) GetNewSerial() *big.Int {
	return s()
}

//DefaultSerialProvider returns a standard simple implementation of a serial number generator
func DefaultSerialProvider(serialPrefix byte) SerialProvider {
	var countingPart = uint64(rand.Int63())
	return SerialProviderFunc(func() *big.Int {
		counter := atomic.AddUint64(&countingPart, 1)
		var randomPart = uint64(rand.Int63())
		byteSlice := make([]byte, 17)
		byteSlice[0] = serialPrefix
		sliceForRandomness := byteSlice[1:9]
		sliceForCounting := byteSlice[9:17]
		binary.BigEndian.PutUint64(sliceForRandomness, randomPart)
		binary.BigEndian.PutUint64(sliceForCounting, counter)
		serialN := &big.Int{}
		serialN.SetBytes(byteSlice)
		return serialN
	})
}
