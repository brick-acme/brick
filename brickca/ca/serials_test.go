/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

package ca_test

import (
	"brick/brickca/ca"
	"math/big"
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

func TestGenerateOneSerial(t *testing.T) {
	s := ca.DefaultSerialProvider(1)
	Convey("When two serials are created", t, func() {
		s1 := s.GetNewSerial()
		s2 := s.GetNewSerial()
		Convey("They should be unequal", func() {
			So(s1.Cmp(s2) == 0, ShouldBeFalse)
		})
	})
}

var result *big.Int

func BenchmarkGenerateSerials(b *testing.B) {
	s := ca.DefaultSerialProvider(2)
	var r *big.Int
	for i := 0; i < b.N; i++ {
		r = s.GetNewSerial()
	}
	result = r
}

func TestGenerateConcurrentSerials(t *testing.T) {
	s := ca.DefaultSerialProvider(1)
	Convey("When we generate multiple parallel serials", t, func() {
		serials := make(chan *big.Int, 10000)
		done := make(chan struct{})
		for i := 1; i < 1000; i++ {
			go func() {
				for j := 1; j < 100; j++ {
					serials <- s.GetNewSerial()
					serials <- s.GetNewSerial()
					serials <- s.GetNewSerial()
				}
				done <- struct{}{}
			}()
		}
		go func() {
			i := 0
			for range done {
				i++
				if i == 999 {
					close(serials)
					break
				}
			}
		}()
		lastSerial := <-serials
		for s := range serials {
			if s.Cmp(lastSerial) == 0 {
				t.Error("Last Serial is equal to current Serial")
				t.Fail()
			}
			lastSerial = s
		}
	})
}
