/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

package ca

import (
	"crypto/x509/pkix"
	"encoding/asn1"
	"fmt"
)

var (
	OidAuthorityInfoAccessOcsp    = asn1.ObjectIdentifier{1, 3, 6, 1, 5, 5, 7, 48, 1}
	OidAuthorityInfoAccessIssuers = asn1.ObjectIdentifier{1, 3, 6, 1, 5, 5, 7, 48, 2}
	OidAuthorityInfoAccess        = asn1.ObjectIdentifier{1, 3, 6, 1, 5, 5, 7, 1, 1}
	OidCRLDistributionPoints      = asn1.ObjectIdentifier{2, 5, 29, 31}
	OidMustStaple                 = asn1.ObjectIdentifier{1, 3, 6, 1, 5, 5, 7, 1, 24}
)

var (
	OidNonce = asn1.ObjectIdentifier{1, 3, 6, 1, 5, 5, 7, 48, 1, 2}
)

//AuthorityInfoAccessDescription - see RFC 5280 4.2.2.1
type AuthorityInfoAccessDescription struct {
	Method   asn1.ObjectIdentifier `asn1:"accessMethod"`
	Location asn1.RawValue         `asn1:"accessLocation"`
}

type OcspMustStapleFeatures struct {
	Features []int `asn1:"Features"`
}

type CRLDistributionPoint struct {
	DistributionPoint DistributionPointName `asn1:"distributionPoint,optional,tag:0"`
}
type DistributionPointName struct {
	FullName []asn1.RawValue `asn1:"fullName,optional,tag:0"`
}

func ConstructAIA(ocspLocation string) pkix.Extension {
	authInfoAccessContent := []AuthorityInfoAccessDescription{
		{
			Method: OidAuthorityInfoAccessOcsp,
			Location: asn1.RawValue{
				Tag:        6,
				Class:      2,
				IsCompound: false,
				Bytes:      []byte(ocspLocation),
			},
		},
		{
			Method: OidAuthorityInfoAccessIssuers,
			Location: asn1.RawValue{
				Tag:        6,
				Class:      2,
				IsCompound: false,
				Bytes:      []byte("testLocation"),
			},
		},
	}
	authInfoAccessContentBytes, err := asn1.Marshal(authInfoAccessContent)
	if err != nil {
		panic(err)
	}

	authorityInfoAccessExtension := pkix.Extension{
		Id:       OidAuthorityInfoAccess,
		Critical: false,
		Value:    authInfoAccessContentBytes,
	}
	return authorityInfoAccessExtension
}

func ConstructMustStaple() pkix.Extension {
	ocspExt := pkix.Extension{
		Id:       OidMustStaple,
		Critical: false,
		Value:    []byte{0x30, 0x03, 0x02, 0x01, 0x05},
	}
	return ocspExt
}

func ConstructCDP(basePath string) pkix.Extension {
	cdpContent := make([]CRLDistributionPoint, 0)
	cdpContent = append(cdpContent, CRLDistributionPoint{
		DistributionPoint: DistributionPointName{
			FullName: []asn1.RawValue{
				asn1.RawValue{
					Tag:        6,
					Class:      2,
					IsCompound: false,
					Bytes:      []byte(fmt.Sprintf("%s/crl", basePath)),
				},
			},
		},
	})

	cdpBytes, err := asn1.Marshal(cdpContent)
	if err != nil {
		panic(err)
	}
	cdpExt := pkix.Extension{
		Id:       OidCRLDistributionPoints,
		Critical: false,
		Value:    cdpBytes,
	}
	return cdpExt
}
