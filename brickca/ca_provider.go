/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

package main

import (
	"context"
	"crypto/x509"
	"crypto/x509/pkix"
	"time"

	"brick/brickca/common"
	"brick/brickweb/acme"
	"brick/core"

	opentracing "github.com/opentracing/opentracing-go"
)

//CaProvider represents a component that is ready to queue up a Certificate Request for issuance
type CaProvider interface {
	QueueCertificateRequest(context.Context, *x509.CertificateRequest, common.Cacert, string) error
	//GetCaCertificateChains returns the Chain to a root Certificate for every CaCert we are willing to issue from
	GetCaCertificateChains(ctx context.Context) [][]*x509.Certificate
	GenerateCRL(context.Context, common.Cacert, []pkix.RevokedCertificate) (*pkix.CertificateList, []byte, error)
	GenerateOCSP(context.Context, *core.Certificate, []byte) ([]byte, error)
	SetAsyncChannel(chan<- *common.CertCompleted)
}

//CertStorage stores passed Certificates for a passed Order
type CertStorage interface {
	StoreCertificate(context.Context, *x509.Certificate, string, string) error
	StoreCaCertificate(context.Context, *x509.Certificate, string, bool) (string, error)
	UpdateOrder(context.Context, *core.Order) error
	CancelOrder(context.Context, string, *acme.ProblemDetails) error
	Check(context.Context) bool
	GetRevokedCerts(ctx context.Context, caID string) ([]pkix.RevokedCertificate, error)
	GetCertificateAndChain(ctx context.Context, certID string) (*core.Certificate, []*x509.Certificate, error)
}

func storeChain(ctx context.Context, s CertStorage, chain []*x509.Certificate) string {
	span, ctx := opentracing.StartSpanFromContext(ctx, "StoreChain")
	defer span.Finish()
	id := ""
	for i := len(chain) - 1; i >= 0; i-- {
		var err error
		willIssue := false
		if i == 0 {
			willIssue = true
		}
		id, err = s.StoreCaCertificate(ctx, chain[i], id, willIssue)
		if err != nil {
			panic(err)
		}
	}

	return id
}

func storeChains(s CertStorage, chains [][]*x509.Certificate) map[*x509.Certificate]string {
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*40)
	defer cancel()
	span, ctx := opentracing.StartSpanFromContext(ctx, "StoreChains")
	defer span.Finish()
	caCertToID := make(map[*x509.Certificate]string)
	for _, chain := range chains {
		if chain == nil || len(chain) == 0 {
			panic("Invalid CaCertChain passed (len 0 or nil")
		}
		id := storeChain(ctx, s, chain)
		caCertToID[chain[0]] = id
	}
	return caCertToID
}
