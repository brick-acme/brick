/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

package consul

import (
	"fmt"

	"github.com/hashicorp/consul/api"
)

func RegisterWithConsul(port int) {
	client, err := api.NewClient(api.DefaultConfig())
	if err != nil {
		panic(err)
	}
	agent := client.Agent()
	err = agent.ServiceRegister(&api.AgentServiceRegistration{
		Kind: api.ServiceKindTypical,
		ID:   "brickca",
		Name: "brickca",
		Port: port,
		Check: &api.AgentServiceCheck{
			Name:       "Grpc-brickca-Check",
			CheckID:    "brickca",
			GRPC:       fmt.Sprintf("%s%d", "127.0.0.1:", port),
			GRPCUseTLS: false,
			Interval:   "10s",
		},
	})
	if err != nil {
		fmt.Println("Cannot register with consul. Is Consul running?")
	}
}
