/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

package completion

import (
	"brick/brickca/common"
	"brick/brickweb/acme"
	"context"
	"crypto/x509"

	"github.com/opentracing/opentracing-go"
)

type certStorage interface {
	StoreCertificate(context.Context, *x509.Certificate, string, string) error
	CancelOrder(context.Context, string, *acme.ProblemDetails) error
}

var store certStorage
var cacerts map[*x509.Certificate]string

//CompleteLoop start
func CompleteLoop(storage certStorage, cacertMap map[*x509.Certificate]string) chan<- *common.CertCompleted {
	if storage == nil {
		panic("Cert Storage is nil")
	}
	cacerts = cacertMap
	store = storage
	ret := make(chan *common.CertCompleted, 5)
	go loop(ret)
	return ret
}

func loop(c <-chan *common.CertCompleted) {
	for req := range c {
		go storeCert(req)
	}
}

func storeCert(c *common.CertCompleted) {
	span := opentracing.SpanFromContext(c.Ctx)
	span = opentracing.StartSpan("StoreCert", opentracing.FollowsFrom(span.Context()))
	ctx := opentracing.ContextWithSpan(context.Background(), span)
	defer span.Finish()
	if c.Err != nil {
		span.SetTag("error", true)
		span.LogKV("event", "error", "error.object", c.Err)
		switch err := c.Err.(type) {
		case *acme.ProblemDetails:
			_ = store.CancelOrder(ctx, c.OrderID, err) //If not even this is working, we really just have to give up
			return
		default:
			_ = store.CancelOrder(ctx, c.OrderID, acme.InternalErrorProblem("An unclear Error occured"))
			return
		}
	}
	cacertID, ok := cacerts[c.CaCert]
	if !ok {
		span.SetTag("error", true)
		span.LogKV("event", "error", "error.message", "Passed CaCert was nil or didn't exist", "error.cacert", c.CaCert)
		store.CancelOrder(ctx, c.OrderID, acme.InternalErrorProblem("An Internal Error occured while issuing the Certificate"))
		return
	}
	span.LogKV("storeCert", c.Cert.SerialNumber.String(), "cacertID", cacertID, "cacn", c.Cert.Issuer.CommonName)
	err := store.StoreCertificate(ctx, c.Cert, c.OrderID, cacertID)
	if err != nil {
		c.Err = err
		storeCert(c) //easy way out
		return
	}
}
